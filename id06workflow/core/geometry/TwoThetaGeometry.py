# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "03/10/2018"


from . import GeometryBase
from id06workflow.core.utils.char import DELTA_CHAR


class TwoThetaGeometry(GeometryBase):
    """
    Simple experimental setup where the variation is only made on two angles
    """

    VERTICAL = 'vertical'
    HORIZONTAL = 'horizontal'

    ORIENTATIONS = (VERTICAL, HORIZONTAL)
    _ORIENTATION_MAPPING = {
        VERTICAL: 'vert',
        HORIZONTAL: 'hori',
    }
    """Dict used when return the geometry"""

    _ALIASES = {
        HORIZONTAL: HORIZONTAL,
        VERTICAL: VERTICAL,
        'hori': HORIZONTAL,
        'vert': VERTICAL
    }

    def __init__(self, twotheta=0.0, xmag=0.0, xpixelsize=0.0, ypixelsize=0.0,
                 orientation=HORIZONTAL):
        name = " ".join(('2', DELTA_CHAR, 'experimental', 'setup'))
        super().__init__(name=name)
        self.twotheta = twotheta # in degree for now, should be in radians
        self.xmag = xmag # don't know about the unit
        self.xpixelsize = xpixelsize # don't know about the unit
        self.ypixelsize = ypixelsize # don't know about the unit
        self.orientation = orientation

    @property
    def twotheta(self):
        return self._twotheta

    @twotheta.setter
    def twotheta(self, value):
        self._twotheta = value

    @property
    def xmag(self):
        return self._xmag

    @xmag.setter
    def xmag(self, value):
        self._xmag = value

    @property
    def orientation(self):
        return self._orientation

    @orientation.setter
    def orientation(self, orientation):
        assert orientation.lower() in self._ALIASES
        self._orientation = self._ALIASES[orientation]

    def is_valid(self):
        return True

    def __eq__(self, other):
        if isinstance(other, TwoThetaGeometry) is False:
            return False
        return (self.twotheta == other.twotheta and
                self.xmag == other.xmag and
                self.xpixelsize == other.xpixelsize and
                self.ypixelsize == other.ypixelsize and
                self.orientation == other.orientation)

    def __str__(self):
        return "\n".join((
                '2' + DELTA_CHAR +': ' + str(self.twotheta),
                'xmag: ' + str(self.xmag),
                'xpixelsize: ' + str(self.xpixelsize),
                'ypixelsize: ' + str(self.ypixelsize),
                'orientation: ' + self.orientation,
        ))
