# coding: utf-8
# ##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "21/11/2018"


import numpy
import logging
_logger = logging.getLogger(__file__)


def getUnique(experiment, key, kind, relative_prev_val=False, cycle_length=None,
              axis=None):
    """Return the set of (unique) value in all the headers
    
    :param str key: the type of value we want
    :param :class: Dataset dataset: the dataset to treat
    :param bool relative_prev_val: In the case we base the dimension on a
                                   relative value (such as diffry) we can have
                                   identical values representing different
                                   positions. So we need to compute the absolute
                                   value of those. And specify it.
                                   This is also why we need the cycle_length
                                   and the axis

    :return set: set of value
    """
    if relative_prev_val is True:
        assert cycle_length is not None and type(cycle_length) is int
        assert axis is not None and type(axis) is int

    metadata = experiment.metadata
    if metadata is None:
        _logger.warning('no metadata found in the given experiment, unable to '
                        'unique value of %s' % key)
        return numpy.array()
    res = list()
    for iFrame, metadata_slice in enumerate(experiment.metadata):
        if (relative_prev_val is True and axis != 0 and
                        iFrame % (cycle_length * axis) != 0):
            continue
        try:
            value = metadata_slice.get_value(kind=kind, name=key)
            if not (isinstance(value, numpy.ndarray) and value.size is 1):
                raise NotImplementedError('Case with values which are not single'
                                          'values ar enot managed')
            else:
                if relative_prev_val is True and len(res) > 0:
                    res.append(value[0] + res[-1])
                else:
                    res.append(value[0])
        except Exception as e:
            _logger.error("fail to load %s, %s cause %s" % (kind, key, e.message))
        if cycle_length is not None and len(numpy.unique(res)) >= cycle_length:
            break
    return numpy.unique(res)
