# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "03/10/2018"


import shutil
import tempfile
import unittest

import numpy
from id06workflow.core.operation import OverwritingOperation, \
    AdditiveOperation

from id06workflow.core.experiment import Experiment
from id06workflow.core.operation import shift
from id06workflow.test import utils


class TestOperation(unittest.TestCase):
    """
    Test operation classes
    """
    def setUp(self):
        unittest.TestCase.setUp(self)
        dims = (100, 100)
        pattern = numpy.zeros(dims)
        background = numpy.random.random(dims)
        self._dir = tempfile.mkdtemp()
        dataset = utils.createDataset(_dir=self._dir,
                                      pattern=pattern,
                                      background=background,
                                      nb_data_files=10,
                                      nb_flat_field_files=1)
        self.experiment = Experiment(dataset=dataset)

    def tearDown(self):
        self.experiment = None
        shutil.rmtree(self._dir)

    def testBaseOperation(self):
        """
        simple test on the basics Operations
        """
        operationOverwrite = OverwritingOperation(experiment=self.experiment,
                                                  name='test')
        operationOverwrite.data_flatten = numpy.random.rand(1, 10, 10)

        additiveOperation = AdditiveOperation(experiment=self.experiment,
                                              name='test')
        with self.assertRaises(Exception):
            additiveOperation.data = numpy.random.rand(1, 10, 10)


class TestCOM(unittest.TestCase):
    """
    Test center of mass operation
    """
    def setUp(self):
        unittest.TestCase.setUp(self)
        dims = (100, 100)
        pattern = numpy.zeros(dims)
        background = numpy.random.random(dims)
        self._dir = tempfile.mkdtemp()
        dataset = utils.createDataset(_dir=self._dir,
                                      pattern=pattern,
                                      background=background,
                                      nb_data_files=10,
                                      nb_flat_field_files=1)
        self.experiment = Experiment(dataset=dataset)

    def tearDown(self):
        self.experiment = None
        shutil.rmtree(self._dir)

    def testResult(self):
        pass


class TestShift(unittest.TestCase):
    """Test shift operation"""
    def setUp(self):
        unittest.TestCase.setUp(self)
        dims = (10, 10)
        pattern = numpy.zeros(dims)
        background = numpy.random.random(dims)
        self._dir = tempfile.mkdtemp()
        dataset = utils.createDataset(_dir=self._dir,
                                      pattern=pattern,
                                      background=background,
                                      nb_data_files=2,
                                      nb_flat_field_files=1)
        self.experiment = Experiment(dataset=dataset)

    def testPracticalResult(self):
        original_data = self.experiment.data.copy()
        operation = shift.Shift(self.experiment, dx=1.0, dy=-1.0, dz=0)

        shifted_data = operation.compute()

        self.assertTrue(numpy.allclose(original_data[1, :-1, 1:],
                                       shifted_data[1, 1:, :-1]))


def suite():
    test_suite = unittest.TestSuite()
    for ui in (TestOperation, TestCOM, TestShift):
        test_suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(ui))
    return test_suite


if __name__ == '__main__':
    unittest.main(defaultTest="suite")
