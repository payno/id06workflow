# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "21/11/2018"


import os
import unittest
from id06workflow.core.operation.mapping import IntensityMapping, \
    GradientRemoval
from id06workflow.core.operation.com import COM
from id06workflow.core.experiment import Dataset, POSITIONER_METADATA
from id06workflow.core.experiment import Experiment, Dim
from id06workflow.core.geometry.TwoThetaGeometry import TwoThetaGeometry
from id06workflow.core.operation import noisereduction
from id06workflow.core.utils import metadata as metadatautils


@unittest.skipIf(os.path.exists('/nobackup/linazimov/payno/datasets/id06/strain_scan') is False, reason='data files not available')
class TestOperationStream(unittest.TestCase):
    """Test some default operation streaming"""
    def setUp(self):
        unittest.TestCase.setUp(self)

        root_folder = '/nobackup/linazimov/payno/datasets/id06/strain_scan'
        data_file_pattern = os.path.join(root_folder, 'reduced_strain/strain_0000.edf')
        assert os.path.exists(data_file_pattern)
        data_bb_files = []
        bb_folder = os.path.join(root_folder, 'bg_ff_5s_1x1')
        for _file in os.listdir(bb_folder):
            data_bb_files.append(os.path.join(bb_folder, _file))
        self.dataset = Dataset(data_files_pattern=data_file_pattern,
                               ff_files=data_bb_files)

        # define geometry
        geometry = TwoThetaGeometry(
                twotheta=0.03,  # for now those are defined in degree but should
                # be defined in radians
                xmag=1.0,  # what is the unit of this ?
                xpixelsize=1.0,
                ypixelsize=1.0,
                orientation=TwoThetaGeometry.VERTICAL)

        self.experiment = Experiment(dataset=self.dataset, geometry=geometry)

    def applyDimsDef(self):
        """Apply experimentation dimension definition"""
        dim1 = Dim(kind=POSITIONER_METADATA, name='diffry',
                   relative_prev_val=True, size=31)
        dim2 = Dim(kind=POSITIONER_METADATA, name='obpitch')
        self.experiment.set_dims(dims={0: dim1, 1: dim2})

    def testDimensionManual(self):
        """Make sure dimensions are well applied"""
        self.assertTrue(len(metadatautils.getUnique(self.experiment,
                                                    kind=POSITIONER_METADATA,
                                                    key='diffry',
                                                    relative_prev_val=True,
                                                    cycle_length=31,
                                                    axis=0
                                                    )) is 31)
        self.assertTrue(len(metadatautils.getUnique(self.experiment,
                                                    kind=POSITIONER_METADATA,
                                                    key='obpitch')) is 2)
        self.assertTrue(len(metadatautils.getUnique(self.experiment,
                                                    kind=POSITIONER_METADATA,
                                                    key='obpitch',
                                                    axis=1,
                                                    cycle_length=2)) is 2)
        self.applyDimsDef()
        self.assertTrue(self.experiment.dims.ndim is 2)
        self.assertTrue(self.experiment.dims.shape == (31, 2))
        self.assertTrue(self.experiment.data.shape == (31, 2, 2048, 2048))

    def testFullStream(self):
        """Apply the entire set of operation on the """
        self.applyDimsDef()
        ope_mask_sub = noisereduction.MaskSubstraction(self.experiment)
        ope_mask_sub.compute()
        ope_background_sub = noisereduction.BackgroundSubtraction(self.experiment)
        ope_background_sub.compute()
        ope_low_threshold = noisereduction.ThresholdRemoval(self.experiment,
                                                            lower=True, threshold=0.2)
        ope_low_threshold.compute()
        ope_high_threshold = noisereduction.ThresholdRemoval(self.experiment,
                                                             lower=False, threshold=10000.0)
        ope_high_threshold.compute()
        ope_intensity_map = IntensityMapping(experiment=self.experiment)
        ope_intensity_map.compute()
        ope_gradient_removal = GradientRemoval(self.experiment)
        ope_gradient_removal.compute()

        com_operation = COM(map=ope_gradient_removal)
        com_operation.compute()
        # TODO: should be call at one moment if the 'project file' is not storing
        # each results
        # self.experiment.save('myProjectFile.h5')


def suite():
    test_suite = unittest.TestSuite()
    for ui in (TestOperationStream, ):
        test_suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(ui))
    return test_suite


if __name__ == '__main__':
    unittest.main(defaultTest="suite")
