# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "03/10/2018"


import os
import unittest

from id06workflow.core.experiment import Experiment, Dataset, \
    POSITIONER_METADATA, Dim
from id06workflow.core.geometry.TwoThetaGeometry import TwoThetaGeometry
from id06workflow.core.operation.mapping import IntensityMapping


@unittest.skipIf(os.path.exists('/nobackup/linazimov/payno/datasets/id06/strain_scan') is False, reason='data files not available')
class TestMappingOperation(unittest.TestCase):
    """
    Test Mapping operation.
    """

    def setUp(self):
        unittest.TestCase.setUp(self)
        # define experiment data
        root_folder = '/nobackup/linazimov/payno/datasets/id06/strain_scan'
        data_file_pattern = os.path.join(root_folder, 'reduced_strain/strain_0000.edf')
        assert os.path.exists(data_file_pattern)
        data_bb_files = []
        bb_folder = os.path.join(root_folder, 'bg_ff_5s_1x1')
        for _file in os.listdir(bb_folder):
            data_bb_files.append(os.path.join(bb_folder, _file))
        self.dataset = Dataset(data_files_pattern=data_file_pattern,
                               ff_files=data_bb_files)

        # define geometry
        geometry = TwoThetaGeometry(
                twotheta=0.03,  # for now those are defined in degree but should
                # be defined in radians
                xmag=1.0,  # what is the unit of this ?
                xpixelsize=1.0,
                ypixelsize=1.0,
                orientation=TwoThetaGeometry.VERTICAL)

        # define experiment
        self.experiment = Experiment(dataset=self.dataset, geometry=geometry)

        # define dimensions
        dim1 = Dim(kind=POSITIONER_METADATA, name='diffry',
                   relative_prev_val=True, size=31)
        dim2 = Dim(kind=POSITIONER_METADATA, name='obpitch')
        self.experiment.set_dims(dims={0: dim1, 1: dim2})

    def testIntensityMapping(self):
        """
        Test IntensityMapping operation
        """
        operation = IntensityMapping(experiment=self.experiment)
        res = operation.compute()
        self.assertTrue(len(res) is 2)
        self.assertTrue(res[0].name == 'diffry')
        self.assertTrue(res[0].kind == POSITIONER_METADATA)
        self.assertTrue(res[1].name == 'obpitch')
        self.assertTrue(res[1].kind == POSITIONER_METADATA)
        self.assertTrue(res[0].mean.shape == (2048, 2048))
        self.assertTrue(res[0].variance.shape == (2048, 2048))
        self.assertTrue(res[0].skewness.shape == (2048, 2048))
        self.assertTrue(res[0].kurtosis.shape == (2048, 2048))
        self.assertTrue(res[1].mean.shape == (2048, 2048))


def suite():
    test_suite = unittest.TestSuite()
    for ui in (TestMappingOperation, ):
        test_suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(ui))
    return test_suite


if __name__ == '__main__':
    unittest.main(defaultTest="suite")
