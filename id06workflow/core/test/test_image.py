# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "20/11/2018"


import os
import unittest
import numpy
from id06workflow.core import image
from id06workflow.core import experiment


class TestShift(unittest.TestCase):
    """
    Test that RefCopy process is correct
    """
    data_dir = '/users/payno/datasets/id06/strain_scan'

    def testShit(self):
        """
        Some stupid test to make sur TwoThetaExpSetup is correctly
        instanciated
        """
        data = numpy.arange(100).reshape(10, 10)
        shifted_data = image.shift_img(data, dx=1.0, dy=-1.0)
        self.assertTrue(numpy.allclose(data[:-1, 1:], shifted_data[1:, :-1]))

    @unittest.skipIf(os.path.exists(data_dir) is False, reason='Dataset source folder is not existing')
    def testShiftGuess(self):
        """Test the algorithm to propose a shift"""

        def createDataset():
            data_file_pattern = os.path.join(self.data_dir,
                                             'reduced_strain/strain_0000.edf')
            assert os.path.exists(data_file_pattern)

            ff_files = []
            dir_ff = os.path.join(self.data_dir, "bg_ff_5s_1x1/")
            [ff_files.append(os.path.join(dir_ff, _file)) for _file in
             os.listdir(dir_ff)]

            dim1 = experiment.Dim(kind=experiment.POSITIONER_METADATA,
                                  name='diffry', relative_prev_val=True,
                                  size=31)
            dim2 = experiment.Dim(kind=experiment.POSITIONER_METADATA,
                                  name='obpitch')
            self._dims = {0: dim1, 1: dim2}

            return experiment.Dataset(data_files_pattern=data_file_pattern,
                                      ff_files=ff_files)

        dataset = createDataset()
        _experiment = experiment.Experiment(dataset=dataset)
        res = (image.guess_shift(data=_experiment.data_flatten, axis=1,
                                 start=-0.0005, stop=0.0005, step=0.0001))
        self.assertTrue(numpy.isclose(res, -2.0e-04))
        res = image.guess_shift(data=_experiment.data_flatten, axis=0,
                                start=-1.0, stop=1.0, step=0.5)
        self.assertTrue(numpy.isclose(res, 0.0))



def suite():
    test_suite = unittest.TestSuite()
    for ui in (TestShift, ):
        test_suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(ui))
    return test_suite


if __name__ == '__main__':
    unittest.main(defaultTest="suite")
