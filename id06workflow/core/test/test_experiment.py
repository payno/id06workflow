# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "09/10/2018"


import unittest
from id06workflow.core.experiment import AcquisitionDims, Dim, DEFAULT_METADATA


class TestDataset(unittest.TestCase):
    """TODO"""
    pass


class TestAcquisitionDims(unittest.TestCase):
    """
    Test the AcquisitionDims API
    """
    def test(self):
        """global test of the AcquisitionDims APi"""
        acquiDim = AcquisitionDims()
        self.assertTrue(acquiDim.shape == (1, ))
        acquiDim.add_dim(axis=0, dim=Dim(kind=DEFAULT_METADATA, name='0', size=10))
        acquiDim.add_dim(axis=1, dim=Dim(kind=DEFAULT_METADATA, name='1'))
        self.assertTrue(acquiDim.shape == (10, -1))
        acquiDim.set_size(axis=1, size=2)
        self.assertTrue(acquiDim.shape == (10, 2))
        acquiDim.clear()
        self.assertTrue(acquiDim.shape == (1, ))
        acquiDim.add_dim(axis=1, dim=Dim(kind=DEFAULT_METADATA, name='0', size=10))
        acquiDim.add_dim(axis=4, dim=Dim(kind=DEFAULT_METADATA, name='1'))
        self.assertTrue(acquiDim.shape == (1, 10, 1, 1, -1))


def suite():
    test_suite = unittest.TestSuite()
    for ui in (TestDataset, TestAcquisitionDims,):
        test_suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(ui))
    return test_suite


if __name__ == '__main__':
    unittest.main(defaultTest="suite")
