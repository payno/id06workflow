# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "08/10/2018"


import unittest

import numpy

from id06workflow.core.operation.noisereduction import MaskSubstraction


class TestMask(unittest.TestCase):
    """
    Test mask application
    """
    def testCylinderMask(self):
        """ Test that the Cylinder mask is well applied. """
        data = numpy.ones((20, 3, 3))
        # TODO: test for other radius
        treated_data = MaskSubstraction.apply_mask(data, mask_type=MaskSubstraction.CYLINDER_MASK, scale=1.0)
        self.assertTrue(numpy.array_equal(treated_data[1], treated_data[5]))
        thRes = numpy.array([[0, 1, 0], [1, 1, 1], [0, 1, 0]])
        self.assertTrue(numpy.array_equal(treated_data[5], thRes))

    def testSphereMask(self):
        """ Test that the Sphere mask is well applied. """
        data = numpy.ones((20, 10, 10))
        treated_data = MaskSubstraction.apply_mask(data, mask_type=MaskSubstraction.SPHERE_MASK, scale=1.0)
        self.assertTrue(numpy.array_equal(treated_data[0], numpy.zeros((10, 10))))
        self.assertTrue(treated_data[9, 5, 5] == 1.0)
        self.assertTrue(treated_data[9, 0, 2] == 0.0)
        self.assertTrue(treated_data[9, 5, 2] == 1.0)


def suite():
    test_suite = unittest.TestSuite()
    for ui in (TestMask, ):
        test_suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(ui))
    return test_suite


if __name__ == '__main__':
    unittest.main(defaultTest="suite")
