# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/


__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "15/10/2018"


from . import AdditiveOperation
from .mapping import _MappingBase, _IMap
from collections import namedtuple
import numpy
import logging

_logger = logging.getLogger(__file__)


_MapsCom = namedtuple('_MapCom', ['map1', 'map2'])


class COM(AdditiveOperation):
    """
    Base class used for mapping
    """

    _NAME = 'com calculation'

    def __init__(self, map):
        assert isinstance(map, _MappingBase)
        AdditiveOperation.__init__(self, experiment=map.experiment,
                                   name=COM._NAME)
        self._map = map
        self.__maps = {}

    def key(self):
        return COM._NAME

    def compute(self):
        self.__maps = {}
        for axis, _map in self._map.dims.items():
            assert _map.mean.ndim is 2
            _mean = _map.mean - numpy.nan_to_num(_map.mean.mean())
            _variance = _map.variance - numpy.nan_to_num(_map.variance.mean())
            _skewness = _map.skewness - numpy.nan_to_num(_map.skewness.mean())
            _kurtosis = _map.kurtosis - numpy.nan_to_num(_map.kurtosis.mean())
            self.__maps[axis] = _IMap(name=_map.name, kind=_map.kind,
                                      mean=_mean, variance=_variance,
                                      skewness=_skewness, kurtosis=_kurtosis)
        self.registerOperation()

    @property
    def maps(self):
        return self.__maps

    @property
    def ndim(self):
        return len(self.__maps)
