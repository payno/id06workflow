# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/


__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "15/10/2018"


from . import OverwritingOperation


class RoiOperation(OverwritingOperation):
    def __init__(self, experiment, origin, size):
        OverwritingOperation.__init__(self, experiment=experiment, name='roi')
        self._origin = origin
        self._size = size
        """
        roi, for now store as a tuple (origin, size). Only deal with rectangle
        roi for now.
        """
        self._cache_data = None
        """ numpy.ndarray used for caching"""

    def key(self):
        return ' '.join((self._name, 'origin:', str(self._origin), 'size:',
                         str(self._size)))

    def compute(self):
        self.data_flatten = self._compute(self.data)
        self.registerOperation()
        return self.data

    def dry_run(self, cache_data=None):
        if cache_data is None:
            self._cache_data = self.data_flatten[...]
        else:
            assert cache_data.ndim is 3
            self._cache_data = cache_data
        return self._compute(self._cache_data)

    def apply(self):
        if self._cache_data is None:
            raise ValueError('No data in cache')
        self.data_flatten = self._cache_data
        self.clear_cache()
        self.registerOperation()
        return self.data

    def _compute(self, data):
        ymin, ymax = int(self._origin[1]), int(self._origin[1] + self._size[1])
        xmin, xmax = int(self._origin[0]), int(self._origin[0] + self._size[0])
        if data.ndim is 3:
            data = data[:, ymin:ymax, xmin:xmax]
        elif data.ndim is 4:
            data = data[:, :, ymin:ymax, xmin:xmax]
        else:
            raise ValueError('data dimension is not managed (%s)' % data.ndim)
        return data

    def clear_cache(self):
        self._cache_data = None
