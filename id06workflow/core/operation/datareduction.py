# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/


__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "15/10/2018"


from . import OverwritingOperation


class DataReduction(OverwritingOperation):
    """
    Set the experiment to use only part of the dataset.
    This operation is a 'one shot' operation ofr now as it is postpone in the
    :class:`Experiment`
    """
    def __init__(self, experiment, x_factor, y_factor, z_factor):
        """
        Apply a data reduction to the experiment

        :param experiment: 
        :param x_factor: 
        :param y_factor: 
        :param z_factor: 
        """
        OverwritingOperation.__init__(self, experiment, name='data reduction')
        self._x_factor = x_factor
        self._y_factor = y_factor
        self._z_factor = z_factor
        self._cache_data = None

    def dry_run(self, cache_data=None):
        raise NotImplementedError('Not possible for data reduction since'
                                  'managed by the Experiment class')

    def compute(self):
        self.registerOperation()
        return self.data

    def apply(self):
        raise NotImplementedError('Not possible for data reduction since'
                                  'managed by the Experiment class')

    def clear_cache(self):
        self._cache_data = None

    def key(self):
        return ' '.join((self._name, 'x:', str(self._x_factor), 'y:',
                         str(self._y_factor), 'z:', str(self._z_factor)))

    @property
    def steps(self):
        return (self._x_factor, self._y_factor, self._z_factor)
