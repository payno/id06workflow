# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/


__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "15/10/2018"

from . import AdditiveOperation
from collections import namedtuple
import numpy
import logging

_logger = logging.getLogger(__file__)


_IMap = namedtuple('_IMap', ['name', 'kind', 'mean', 'variance', 'skewness',
                             'kurtosis'])


class _MappingBase(AdditiveOperation):
    """
    Base class used for mapping
    """
    @property
    def dims(self):
        """

        :return:
        :rtype: dict with axis index as key, _IMap as value
        """
        raise NotImplementedError('Base class')

    @property
    def intensity_map(self):
        raise NotImplementedError('Base class')

    @property
    def ndim(self):
        raise NotImplementedError('Base class')


class IntensityMapping(_MappingBase):
    """
    for each detector pixel (x,y), each data dimension (motors scanned/rocked)
    are evaluated: 1) intensity is summed along all other dimensions than the
    active one 2) maximum momentum method used to find peak position,
    variance and skewness 3) these are saved in Experiment.map
    
    :param float or None threshold: if the threshold is None then the mean
                                value of the current data will be take
    """
    KEY = 'intensity mapping'

    def __init__(self, experiment, threshold=None):
        _MappingBase.__init__(self, experiment=experiment, name=self.KEY)
        self._dim = []
        if threshold is None:
            self._threshold = numpy.median(self.data)
        else:
            self._threshold = threshold

        self.__intensity_map = None
        self.__dim = {}
        """Associate to each dimension the _IMap"""

    def key(self):
        return self.KEY

    def compute(self):
        _logger.info('start computing the dx2d map')

        if self.data is None:
            _logger.warning('Experiment as no data defined, unable to apply'
                            'mapping')
            return None
        else:
            assert self.data.ndim > 2

            # TODO: avoid recomputing several time the intensity map
            self._resetIntensityMap()
            self._resetDim()
            for x in range(self.data.shape[-1]):
                self.updateProgress(int(x / self.data.shape[-1] * 100.0))
                for y in range(self.data.shape[-2]):
                    # TODO: do this operation using a grid or something close
                    # but would have to find a way to write at the same position...
                    reciprocalVol = numpy.squeeze(self.data_flatten[:, y, x]).astype(numpy.float128)
                    reciprocalVol = reciprocalVol - reciprocalVol.min()
                    maxIntensity = reciprocalVol.max()
                    if maxIntensity > self._threshold:
                        self.__intensity_map[y, x] = maxIntensity
                        # Here: TODO, later when can have several dimension (motors scanned/rockers)
                        # do it for all dims
                        for axis, dim in self._experiment.dims:
                            intensity = numpy.squeeze(reciprocalVol.sum())
                            # TODO: move calculation of angle_mean, angle_variance,
                            dim_unique_values = dim.unique_values.astype(numpy.float128)
                            # TODO: why mean is obtained by the sum ?
                            # angles_mean = (intensity * angles).mean()
                            _mean = (intensity * dim_unique_values).sum()
                            diff = (dim_unique_values - _mean)
                            _variance = (intensity * diff ** 2).sum()
                            _skewness = (intensity * diff ** 3).sum()
                            _kurtosis = (intensity * diff ** 4).sum()
                            self.dims[axis].mean[y, x] = _mean
                            self.dims[axis].variance[y, x] = _variance
                            self.dims[axis].skewness[y, x] = _skewness
                            self.dims[axis].kurtosis[y, x] = _kurtosis
            self.updateProgress(100)
            self.registerOperation()
            return self.dims

    def _resetIntensityMap(self):
        self.__intensity_map = numpy.zeros(self.data.shape[-2:], dtype=numpy.float128)

    def _resetDim(self):
        self.__dim = {}
        for axis, dim in self._experiment.dims:
            _map = _IMap(
                    name=dim.name,
                    kind=dim.kind,
                    mean=numpy.zeros((self.data.shape[-2:])),
                    variance=numpy.zeros((self.data.shape[-2:])),
                    skewness=numpy.zeros((self.data.shape[-2:])),
                    kurtosis=numpy.zeros((self.data.shape[-2:])))
            self.__dim[axis] = _map

    @property
    def intensity_map(self):
        return self.__intensity_map

    @property
    def dims(self):
        assert type(self.__dim) is dict
        return self.__dim

    @property
    def ndim(self):
        return len(self.__dim)


class GradientRemoval(_MappingBase):
    """
    Compute gradient for the current intensity and remove it from the initial
    intensity calculation
    """
    def __init__(self, experiment):
        _MappingBase.__init__(self, experiment=experiment,
                              name='gradient removal')
        self.__dim = {}
        self.__intensity_map = None

    def dry_run(self, cache_data=None):
        raise ValueError('Not available for Gradient removal yet')

    def compute(self):
        # TODO: for now we ask experiment for the intensity mapping if computed
        mapping = self._experiment.getOperation(IntensityMapping.KEY)
        if mapping is None:
            _logger.error('unable to process gradient correction, mapping '
                          'needs to be proceed before.')
            return

        self.__dim = self.apply_gradient_correction(self.data_flatten,
                                                    mapping=mapping)
        self.__intensity_map = mapping.intensity_map
        self.registerOperation()

    def apply_gradient_correction(self, data, mapping):
        assert isinstance(mapping, IntensityMapping)
        _gradients = {}

        for axis, dim in mapping.dims.items():
            unique_values = numpy.array(self._experiment.dims.get(axis).unique_values)
            _delta = unique_values.max() - unique_values.min()
            _logger.info('delta for axis %s: %s' % (axis, _delta))
            corr2, corr1 = numpy.meshgrid(
                    numpy.linspace(-_delta, _delta, data.shape[2]),
                    numpy.linspace(0, 0, data.shape[1]))

            _mean = dim.mean + corr2
            _variance = dim.variance + corr2
            _skewness = dim.skewness + corr2
            _kurtosis = dim.kurtosis + corr2
            gradient = _IMap(mean=_mean, variance=_variance, skewness=_skewness,
                             kurtosis=_kurtosis, name=dim.name, kind=dim.kind)

            _gradients[axis] = gradient

        return _gradients

    def key(self):
        return self._name

    @property
    def dims(self):
        assert type(self.__dim) is dict
        return self.__dim

    @property
    def intensity_map(self):
        return self.__intensity_map

    @property
    def ndim(self):
        return len(self.__dim)
