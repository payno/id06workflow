# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/


__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "15/10/2018"


from silx.gui import qt


class _BaseOperation(qt.QObject):
    """
    Operation to be applied on an experiment
    """
    # TODO would be nice to have required operations to be processed before this
    # one
    sigProgress = qt.Signal(int)
    """Advancement of the operation in percentage"""

    def __init__(self, experiment, name, can_overwrite_data=False):
        """
        
        :param :class:`Experiment` experiment: experiment concerned by the
                                               operation
        :param name: bame of the operation
        :param bool can_overwrite_data: If True then can overwrite
                                        experiment.data 
        :param must_be_unique: If true then can only be applied once to the
                               experiment
        """
        qt.QObject.__init__(self)
        self._name = name
        self._experiment = experiment
        self._can_overwrite_data = can_overwrite_data

    @property
    def data(self):
        data = self._experiment.data.view()
        data.setflags(write=self._can_overwrite_data)
        return data

    @property
    def data_flatten(self):
        data_flatten = self._experiment.data_flatten.view()
        data_flatten.setflags(write=self._can_overwrite_data)
        return data_flatten

    @property
    def experiment(self):
        return self._experiment

    def compute(self):
        raise NotImplementedError('Base class')

    def key(self):
        """
        
        :return: key used to define the operation
        :rtype: str
        """
        raise NotImplementedError('Base class')

    def registerOperation(self):
        self._experiment.addOperation(self)

    def updateProgress(self, progress):
        """

        :param float progress: percentage of advancement
        """
        assert type(progress) is int
        self.sigProgress.emit(progress)


class OverwritingOperation(_BaseOperation):
    """
    Operation that can effect the experiment __data
    """
    def __init__(self, experiment, name):
        _BaseOperation.__init__(self, experiment, name, can_overwrite_data=True)

    @_BaseOperation.data_flatten.setter
    def data_flatten(self, data):
        self._experiment.data_flatten = data

    def dry_run(self, cache_data=None):
        """
        Apply the data on a different experiement/ dataset...
        To be used before applying it
        
        :param cache_data: some existing cache data if any
        """
        raise NotImplementedError('Base class')

    def apply(self):
        """
        If dry ru nhas been called then can use data in cache to apply the
        operation
        """
        raise NotImplementedError('Base class')

    def clear_cache(self):
        """
        Clear the cache used for the dry_run
        """
        raise NotImplementedError('Base class')


class AdditiveOperation(_BaseOperation):
    """
    Operation that cannot effect the experiment __data
    """
    def __init__(self, experiment, name):
        _BaseOperation.__init__(self, experiment, name, can_overwrite_data=False)
