# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/


__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "15/10/2018"


import logging
from . import OverwritingOperation
import numpy

_logger = logging.getLogger(__file__)


class ThresholdRemoval(OverwritingOperation):
    """
    Apply a simple threshold filtering on data
    """
    def __init__(self, experiment, lower, threshold=None,
                 replacement_value=None):
        OverwritingOperation.__init__(self, experiment=experiment,
                                      name='threshold removal')
        assert type(lower) is bool

        self._threshold = threshold
        self._lower = lower
        self._replacement_value = threshold if replacement_value is None else replacement_value
        self._cache_data = None

    def key(self):
        _operator = '<' if self._lower is True else '>'
        return ' '.join((self._name, _operator, str(self._threshold), 'by',
                         str(self._replacement_value)))

    @property
    def lower(self):
        return self._lower

    @lower.setter
    def lower(self, value):
        self._lower = value

    @property
    def threshold(self):
        return self._threshold

    @threshold.setter
    def threshold(self, threshold):
        assert type(threshold) in (float, int, None)
        self._threshold = threshold

    @property
    def replacement_value(self):
        return self._replacement_value

    @replacement_value.setter
    def replacement_value(self, value):
        self._replacement_value = value

    def compute(self):
        """

        :param float or int threshold: the value of the threshod to apply
        :param bool lower: if True, apply the replacement_value to all the value
                           below the threshold. If false apply the
                           replacement_value to all the value above the
                           threshold
        :param replacement_value: value to set to pixel failing the condition
                                  if None given, will set the value of the
                                  threshold
        """
        _res = self._compute(self.data_flatten)
        if _res is not None:
            self.data_flatten = _res
            self.registerOperation()
            return self.data_flatten

    def dry_run(self, cache_data=None):
        if cache_data is None:
            self._cache_data = self.data_flatten[...]
        else:
            self._cache_data = cache_data
        return self._compute(self._cache_data)

    def apply(self):
        if self._cache_data is None:
            raise ValueError('No data in cache')
        self.data_flatten = self._cache_data
        self.registerOperation()
        self._cache_data = None

    def _compute(self, data):
        if data is None:
            _logger.warning('No existing data, cannot apply threshold')
        else:
            if self._replacement_value is None:
                self._replacement_value = self._threshold
            if self._lower is True:
                data[data < self._threshold] = self._replacement_value
            else:
                data[data > self._threshold] = self._replacement_value
        return data

    def clear_cache(self):
        self._cache_data = None


class BackgroundSubtraction(OverwritingOperation):
    """
    Apply a simple background subtraction by removing the median of all the
    layer
    """
    def __init__(self, experiment):
        OverwritingOperation.__init__(self, experiment=experiment,
                                      name='background subtraction')
        self._cache_data = None

    def key(self):
        return self._name

    def compute(self):
        _data_sub = self._compute(self.data_flatten)
        if _data_sub is not None:
            assert _data_sub.ndim is 3
            self.data_flatten = _data_sub
            self.registerOperation()
        return self.data_flatten

    def dry_run(self, cache_data=None):
        if cache_data is None:
            self._cache_data = self.data_flatten[...]
        else:
            self._cache_data = cache_data
        self._cache_data = self._compute(self._cache_data)
        return self._cache_data

    def apply(self):
        if self._cache_data is None:
            raise ValueError('No data in cache')
        self.data_flatten = self._cache_data
        self.clear_cache()
        self.registerOperation()
        return self.data_flatten

    def _compute(self, data):
        if data is None:
            _logger.warning('No existing data, cannot apply background '
                            'subtraction')
            return None
        else:
            background = self._experiment.getMedianBackground()
            if isinstance(background, numpy.ndarray):
                assert data.ndim is 3
                assert background.ndim is 2
                data[:] = data[:] - background
            else:
                data = data - background
        return numpy.nan_to_num(data, copy=False)

    def clear_cache(self):
        self._cache_data = None


class MaskSubstraction(OverwritingOperation):
    """
    Apply a mask on the dataset
    For sphere will take the inscribed sphere

    :param numpy.ndarray data: 
    :param str mask_type: 
    :param float scale:     
    """

    SPHERE_MASK = 'sphere'
    CYLINDER_MASK = 'cylinder'

    MASK_TYPES = (CYLINDER_MASK, SPHERE_MASK)

    def __init__(self, experiment, mask_type=CYLINDER_MASK, scale=1.0):
        OverwritingOperation.__init__(self, experiment=experiment,
                                      name='Mask subtraction')
        self._cache_data = None
        self._mask_type = mask_type
        self._scale = scale

    @property
    def scale(self):
        return self._scale

    @scale.setter
    def scale(self, scale):
        self._scale = scale

    @property
    def mask_type(self):
        return self._mask_type

    @mask_type.setter
    def mask_type(self, type_):
        self._mask_type = type_

    def key(self):
        return ' '.join((self._name, 'type:', str(self._mask_type),
                         'scale:', str(self._scale)))

    def compute(self):
        self._compute(self._experiment.data_flatten)
        self.registerOperation()
        return self._experiment.data_flatten

    def clear_cache(self):
        self._cache_data = None

    def dry_run(self, cache_data=None):
        if cache_data is None:
            assert cache_data.ndim is 3
            self._cache_data = self._experiment.data_flatten[...]
        else:
            self._cache_data = cache_data
        return self._compute(self._cache_data)

    def apply(self):
        if self._cache_data is None:
            raise ValueError('No data in cache')
        self.registerOperation()
        return self._experiment.data_flatten

    def _compute(self, data):
        assert data.ndim is 3
        return self.apply_mask(data, self.mask_type, self.scale)

    @staticmethod
    def apply_mask(data, mask_type, scale):
        assert scale > 0
        res = data.copy()
        if mask_type == MaskSubstraction.SPHERE_MASK:
            radius = min(data.shape) // 2
            radii = MaskSubstraction._compute_radius_to_center(data)
            res[radii > radius * scale] = 0.0
            return res
        elif mask_type == MaskSubstraction.CYLINDER_MASK:
            # For now the cylinder axis is y
            radius = min(data.shape[1:]) // 2
            radii = MaskSubstraction._compute_radius_to_y_axis(data)
            res = data.copy()
            res[:, radii > radius * scale] = 0.0
            return res
        else:
            raise NotImplementedError()

    @staticmethod
    def _compute_radius_to_center(data):
        assert data.ndim is 3
        xcenter = (data.shape[2]) // 2
        ycenter = (data.shape[1]) // 2
        zcenter = (data.shape[0]) // 2
        z, y, x = numpy.ogrid[:data.shape[0], :data.shape[1], :data.shape[2]]
        r = numpy.sqrt(
            (x - xcenter) ** 2 + (y - ycenter) ** 2 + (z - zcenter) ** 2)
        return r

    @staticmethod
    def _compute_radius_to_y_axis(data):
        assert data.ndim is 3
        xcenter = (data.shape[2]) // 2
        ycenter = (data.shape[1]) // 2
        y, x = numpy.ogrid[:data.shape[1], :data.shape[2]]
        r = numpy.sqrt((x - xcenter) ** 2 + (y - ycenter) ** 2)
        return r
