# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/


__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "01/10/2018"

try:
    from fabio.fabio import file_series
except ImportError:
    from id06workflow.third_party.fabio.file_serie import FileSeries, filename_series
import numpy
import os


class Dataset(object):
    """
    Class used to define a dataset

    :param str or None data_files_pattern: pattern to the data files
                                           data_0000.edf for example
    :param list or tuple or None dark_files: list of the dark files
    :param list or tuple or None ff_files: list of the flat field files
    """
    def __init__(self, data_files_pattern=None, dark_files=None, ff_files=None):
        assert isinstance(data_files_pattern, (type(None), str, set))
        assert isinstance(dark_files, (type(None), list, set))
        assert isinstance(ff_files, (type(None), list, set))
        self._data_files_pattern = data_files_pattern
        """data files"""
        self._data = None
        """data"""
        self._dark_files = [] if dark_files is None else dark_files
        """dark files"""
        self._ff_files = [] if ff_files is None else ff_files
        """flat field files"""
        self.__data_has_changed = data_files_pattern is not None
        """Flag to avoid loading data from files every time"""
        self.__dark_has_changed = dark_files is not None
        """Flag to avoid loading darks from files every time"""
        self.__ff_has_changed = ff_files is not None
        """Flag to avoid loading flat field from files every time"""

        """Region Of Interest if any defined"""
        self.__data_series = None
        self.__darks = None
        self.__ff = None

    @property
    def data_files_pattern(self):
        return self._data_files_pattern

    @data_files_pattern.setter
    def data_files_pattern(self, data_files):
        self._data_files_pattern = data_files
        self.__data_has_changed = True

    @property
    def dark_files(self):
        return self._dark_files

    @dark_files.setter
    def dark_files(self, dark_files):
        self._dark_files = dark_files
        self.__dark_has_changed = True

    def addDarkFile(self, dark_file):
        self._dark_files.append(dark_file)
        self.__dark_has_changed = True

    @property
    def flat_fields_files(self):
        return self._ff_files

    @flat_fields_files.setter
    def flat_fields_files(self, flat_fields_files):
        self._ff_files = flat_fields_files
        self.__ff_has_changed = True

    def addFlatFieldFile(self, flat_field_file):
        self._ff_files.append(flat_field_file)
        self.__ff_has_changed = True

    def is_valid(self):
        return len(self._data_files_pattern) > 0 or self._data is not None

    def __eq__(self, other):
        if isinstance(other, Dataset) is False:
            return False
        return (sorted(list(self.data_files_pattern)) == sorted(list(other.data_files_pattern)) and
                sorted(list(self.dark_files)) == sorted(list(other.dark_files)) and
                sorted(list(self.flat_fields_files)) == sorted(list(other.flat_fields_files)))

    def __str__(self):
        return "\n".join(
                ("data_files_pattern: " + (str(self.data_files_pattern) or ""),
                "dark_files: " + (str(self.dark_files) or ""),
                "flat field images: " + (str(self.flat_fields_files) or "")))

    def getBackground(self):
        if self.__ff_has_changed is True:
            self.__ff = self._loadFiles(self.flat_fields_files)
            self.__ff_has_changed = False
        return self.__ff

    def getDataFileSeries(self):
        """
        Return a fabio :FileSeries: to iterate over frame.
        """
        if self.__data_has_changed is True:
            if not os.path.exists(self.data_files_pattern):
                raise ValueError('Given file path does not exists (%s)' % self.data_files_pattern)

            filenames = filename_series(self.data_files_pattern)
            self.__data_series = FileSeries(filenames=filenames,
                                            single_frame=True)
            self.__data_has_changed = False
        return self.__data_series

    def _loadFiles(self, files_list):
        # TODO: add information if the number of frame is fixed...
        if len(files_list) is 0:
            return None

        res = []
        series = FileSeries(filenames=files_list, single_frame=True)
        for iframe in range(series.nframes):
            res.append(series.getframe(iframe).data)
        return numpy.asarray(res)
