# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2015-2016 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

from __future__ import absolute_import

__authors__ = ["H> Payno"]
__license__ = "MIT"
__date__ = "14/11/2018"


import logging
import numpy

_logger = logging.getLogger(__file__)


def shift_img(data, dx, dy):
    """
    Apply simple 2d image shift.
    
    :param numpy.ndarray data: 
    :param dx: x translation to be applied
    :param dy: y translation to be applied
    
    :return: shifted image
    :rtype: numpy.ndarray
    """
    assert data.ndim is 2
    _logger.info('apply shift dx=%s, dy=%s ' % (dx, dy))
    ynum, xnum = data.shape
    xmin = int(-numpy.fix(xnum / 2))
    xmax = int(numpy.ceil(xnum / 2) - 1)
    ymin = int(-numpy.fix(ynum / 2))
    ymax = int(numpy.ceil(ynum / 2) - 1)

    nx, ny = numpy.meshgrid(numpy.linspace(xmin, xmax, xnum),
                            numpy.linspace(ymin, ymax, ynum))

    res = abs(numpy.fft.ifft2(numpy.fft.fft2(data) * numpy.exp(
            1.0j * 2.0 * numpy.pi * (dy * ny / ynum + dx * nx / xnum))))
    return res


def guess_shift(data, axis, start=-1.0, stop=1.0, step=0.1):
    """Try to guess the shift from the flatten data"""

    def com_1d(ydata):
        xdata = numpy.arange(len(ydata))
        deno = numpy.sum(ydata).astype(numpy.float32)
        if deno == 0.:
            return numpy.nan
        else:
            return numpy.sum(xdata * ydata).astype(numpy.float32) / deno

    def com_2d(data, axis):
        sum = numpy.sum(data, axis=axis)
        deno = numpy.sum(sum)
        if deno == 0.0:
            return numpy.nan
        else:
            return sum / deno

    def com_var(x_shift, y_shift):
        if x_shift is None:
            assert y_shift is not None
        if y_shift is None:
            assert x_shift is not None

        coms = []
        for iImg, img in enumerate(data):
            # if img is too large, reduce size
            _img = img
            if _img.shape[-1] > 1024:
                _img = _img[::2, ::2]

            if x_shift is None:
                shifted_img = shift_img(_img, dx=0.0, dy=y_shift * iImg)
            else:
                shifted_img = shift_img(_img, dx=x_shift * iImg, dy=0.0)

            if x_shift is None:
                com = com_1d(ydata=com_2d(shifted_img, axis=1))
            else:
                com = com_1d(ydata=com_2d(shifted_img, axis=0))

            if img.shape[-1] > 1024:
                com = com * 0.5
            coms.append(com)
        return numpy.var(coms)

    assert axis in (0, 1)
    assert data.ndim is 3
    vars = []
    _range = numpy.arange(start=start, stop=stop, step=step)
    if axis is 0:
        y_shift = None
        [vars.append(com_var(x_shift, y_shift)) for x_shift in _range]
    elif axis is 1:
        x_shift = None
        [vars.append(com_var(x_shift, y_shift)) for y_shift in _range]
    else:
        raise not NotImplementedError('')
    return _range[numpy.argmin(vars)]
