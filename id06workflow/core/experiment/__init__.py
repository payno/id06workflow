# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/


__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "01/10/2018"

import logging
from collections import OrderedDict
from silx.io import fabioh5
from silx.io.fabioh5 import FabioReader

import numpy
from id06workflow.core.operation import _BaseOperation
from id06workflow.core.operation.datareduction import DataReduction

from id06workflow.core.Dataset import Dataset
from id06workflow.core.geometry import GeometryBase
from id06workflow.core.geometry.TwoThetaGeometry import TwoThetaGeometry
from id06workflow.core.operation.roi import RoiOperation
from id06workflow.core.utils import metadata as metadatautils

_logger = logging.getLogger(__file__)

DEFAULT_METADATA = FabioReader.DEFAULT

COUNTER_METADATA = FabioReader.COUNTER

POSITIONER_METADATA = FabioReader.POSITIONER

_METADATA_TYPES = {
    'default': DEFAULT_METADATA,
    'counter': COUNTER_METADATA,
    'positioner': POSITIONER_METADATA,
}

_METADATA_TYPES_I = {}
"""used to retrieve the metadata name (str) for the silx.io.fabioh5 id"""
for key, value in _METADATA_TYPES.items():
    assert value not in _METADATA_TYPES_I
    _METADATA_TYPES_I[value] = key


class Experiment(object):
    """
    Define all component of an experiment:

    - :class:`Dataset`
    - :class:`Geometry`
    """

    def __init__(self, dataset=None, geometry=None):
        if dataset is not None:
            assert isinstance(dataset, Dataset)
        if geometry is not None:
            assert isinstance(geometry, GeometryBase)
        self.dataset = dataset
        self.geometry = geometry

        self.__data = None
        """current data. The one updated with treatment, roi...
        Which is different from the raw data contained in the dataset"""
        self.__metadata = None
        """Metadata associated to the data. One per slice edf file header"""
        self.__dims = AcquisitionDims()
        """Number of dimension of the experiment (motors scanned/rocked)
        For now limited to 1
        """
        self._operation_stack = OrderedDict()
        """List all the operations applied don the raw data"""
        self._rois_operations = []
        """List all the operations relative to roi on raw data (duplicated
        from operation stack)"""
        self._data_reduction_operations = []
        """List all the operations relative to data reduction on raw data
        (duplicated from operation stack)"""

    @property
    def dataset(self):
        return self._dataset

    @dataset.setter
    def dataset(self, dataset):
        assert dataset is None or isinstance(dataset, Dataset)
        self._dataset = dataset

    @property
    def geometry(self):
        return self._geometry

    @geometry.setter
    def geometry(self, geometry):
        assert geometry is None or isinstance(geometry, GeometryBase)
        self._geometry = geometry

    @property
    def metadata(self):
        if self.__metadata is None:
            self.data
        return self.__metadata

    @property
    def roi(self):
        # TODO: manage several rois
        rois = self._getRoiOperations()
        if len(rois) is 0:
            return None
        elif len(rois) is 1:
            return rois[0]._origin, rois[0]._size
        else:
            raise NotImplementedError('Cannot deal with several ROI yet')

    @property
    def dims(self):
        return self.__dims

    def getRawData(self):
        # TODO: cache should probably be used in the future to deal wuth data
        if self.dataset is None:
            return None
        fileseries = self.dataset.getDataFileSeries()
        reductionStep = self._getDataReductionOperations()
        if reductionStep in (None, []):
            reductionStep = [(1, 1, 1)]
        if len(reductionStep) is 1:
            if isinstance(reductionStep[0], DataReduction):
                steps = reductionStep[0].steps
            else:
                steps = reductionStep[0]
        else:
            raise NotImplementedError('cannot manage several reduction steps for the moment')

        # load only files we want to keep. Apply the z reduction
        data_with_z_reduction, self.__metadata = self._loadFileSeries(fileseries=fileseries,
                                                                      z_step=steps[2])
        if self.roi is not None:
            assert isinstance(self.roi, tuple)
            origin, size = self.roi
            assert data_with_z_reduction.ndim is 3
            # TODO: make sure origin and size are given as y, x
            ymin, ymax = int(origin[1]), int(origin[1] + size[1])
            xmin, xmax = int(origin[0]), int(origin[0] + size[0])
            return data_with_z_reduction[:, ymin:ymax:steps[1], xmin:xmax:steps[0]]
        else:
            return data_with_z_reduction[:, ::steps[1], ::steps[0]]

    def _loadFileSeries(self, fileseries, z_step):
        """This will only deal with .edf file for now"""
        data = []
        headers = []
        for iFrame in numpy.arange(start=0, stop=fileseries.nframes, step=z_step):
            # TODO: deal with motors
            frame = fileseries.getframe(iFrame)
            data.append(frame.data)
            headers.append(fabioh5.EdfFabioReader(fabio_image=frame))
        return numpy.asarray(data), headers

    @property
    def data(self):
        if self.__dims.ndim > 1:
            # TODO: create view or adapt view on raw data
            shape = list(self.__dims.shape)
            shape.append(self.data_flatten.shape[-2])
            shape.append(self.data_flatten.shape[-1])
            return self.data_flatten.view().reshape(shape)
        else:
            return self.data_flatten

    @property
    def data_flatten(self):
        if self.__data is None:
            self.__data = self.getRawData()
        return self.__data

    @data_flatten.setter
    def data_flatten(self, data):
        assert data.ndim > 2
        self.__data = data.reshape(-1, data.shape[-2], data.shape[-1])

    @property
    def ndim(self):
        """

        :return: number of dimension in the experiement (so do not include the 2
                 dimensions form the frame)
        :rtype: int
        """
        return self.__dims.ndim

    @property
    def background_subtracted(self):
        return self.hasOperation('background substraction')

    @background_subtracted.setter
    def background_subtracted(self, substracted):
        self._background_subtracted = substracted

    @property
    def angles(self):
        # TODO: cache angles array ?
        if self.geometry is None:
            _logger.warning('No geometry defined')
            return None
        elif isinstance(self.geometry, TwoThetaGeometry):
            # TODO: not sure about the way to compute angles. Different in the matlab script.
            return numpy.linspace(start=-self.geometry.twotheta,
                                  stop=self.geometry.twotheta,
                                  num=self.data.shape[0])
        else:
            raise NotImplementedError('Geometry type not managed')

    @property
    def operations(self):
        return self._operation_stack

    def getMedianBackground(self):
        """

        :return: the background of the dataset, clamp to the roi if any defined
        :rtype single value or numpy.ndarray
        """
        background = self.dataset.getBackground()

        # case background is the mean value of the current data
        if background is None:
            if self.__data is not None:
                _logger.warning(
                    'No flat field defined for background, getting it '
                    'directly from data files')
                return numpy.median(numpy.nan_to_num(self.data_flatten))
            else:
                return None
        else:
            # case background is the mean of flat fields
            reductionStep = self._getDataReductionOperations()
            if reductionStep in (None, []):
                steps = (1, 1, 1)
            else:
                if len(reductionStep) is 1:
                    steps = reductionStep[0].steps
                else:
                    raise NotImplementedError(
                        'cannot manage several reduction steps for the moment')
            # first apply data reduction because roi is applied after data
            # reduction
            background_img = numpy.median(background, axis=0)
            background_img = background_img[::steps[1], ::steps[0]]

            if self.roi is None:
                return background_img
            else:
                origin, size = self.roi
                ymin, ymax = int(origin[1]), int(origin[1] + size[1])
                xmin, xmax = int(origin[0]), int(origin[0] + size[0])
                return background_img[ymin:ymax, xmin:xmax]

    def addOperation(self, operation):
        """Add the operation to the stack of operations"""
        _logger.info('register operation %s' % operation.key())
        assert isinstance(operation, _BaseOperation)
        if operation.key() in self._operation_stack:
            _logger.warning('operation %s has been processed several time' % operation.key())
        self._operation_stack[operation.key()] = operation
        if isinstance(operation, RoiOperation):
            self._rois_operations.append(operation)
        if isinstance(operation, DataReduction):
            self._data_reduction_operations.append(operation)

    def getOperation(self, id):
        if id in self.operations:
            return self.operations[id]
        else:
            return None

    def _getRoiOperations(self):
        """
        
        :return: the list of roi operation
        """
        return self._rois_operations

    def _getDataReductionOperations(self):
        """

        :return: the list of roi operation.

        ..note: reduction steps are defined as tuple(x, y, z)
        """
        return self._data_reduction_operations

    def hasOperation(self, operationID):
        raise NotImplementedError('Not implemented yet')

    @property
    def nslices(self):
        if self.data_flatten is None:
            return 0
        else:
            assert self.data_flatten.ndim is 3
            return self.data_flatten.shape[0]

    def set_dims(self, dims):
        assert isinstance(dims, dict)
        self.__dims.clear()
        _fail = False, None  # register fail status and latest reason
        for axis, dim in dims.items():
            try:
                unique_dim_value = metadatautils.getUnique(self,
                                                           kind=dim.kind,
                                                           key=dim.name,
                                                           relative_prev_val=dim.relative_prev_val,
                                                           cycle_length=dim.size,
                                                           axis=axis)
            except Exception as e:
                if _fail[0] is False:
                    _fail = True, e
            else:
                if dim.size is None:
                    dim._setSize(size=len(unique_dim_value))
                dim._setUniqueValues(unique_dim_value)

            self.__dims.add_dim(axis=axis, dim=dim)

        if _fail[0] is True:
            raise ValueError('Fail to define all diemnsion size. Latest error first error was %s' % _fail[1])


class AcquisitionDims(object):
    """
    Define the view of the data which has to be made
    """
    def __init__(self):
        self.__dims = {}

    def add_dim(self, axis, dim):
        assert isinstance(dim, Dim)
        self.__dims[axis] = dim

    def clear(self):
        self.__dims = {}

    @property
    def ndim(self):
        max_index = -1
        for index in self.__dims.keys():
            max_index = max(max_index, index)
        return max(max_index, 0) + 1

    def get(self, axis):
        """

        :param int axis: axis for which we want the dimension definition
        :return: the requested dim if exists
        """
        assert type(axis) is int
        if axis in self.__dims:
            return self.__dims[axis]
        else:
            return None

    @property
    def shape(self):
        """
        
        :return: shape of the currently defined dims
        """
        shape = []
        for iDim in range(self.ndim):
            if iDim not in self.__dims:
                shape.append(1)
            else:
                shape.append(self.__dims[iDim].size or -1)
        return tuple(shape)

    def set_size(self, axis, size):
        if axis not in self.__dims:
            _logger.error('axis %s is not defined yet, cannot defined a size '
                          'for it' % axis)
        else:
            self.__dims[axis] = Dim(name=self.__dims[axis].name,
                                    kind=self.__dims[axis].kind,
                                    size=size)

    def __iter__(self):
        for iAxis, dim in self.__dims.items():
            yield (iAxis, dim)


class Dim(object):
    def __init__(self, kind, name, size=None, relative_prev_val=False):
        """
        Define a dimension used during the experiment

        :param int or str kind: where the ket can be found in fabioh5 mapping
        :param str name: name of the dimension (should fit the fabioh5 mapping
                         for now)
        :param int or None size: length of the dimension.
        :param relative_prev_val: if the parameter defining the dimension
                                  (kind + key) is defined by a relative value.
                                  'diffry' for example is a relative position.
                                  For dimension we need to retrieve unique
                                  values so in those cases we need to apply
                                  a post processing and get this information
                                  and length of the acquisition (passed 
                                  by size here)
        :type relative_prev_val: bool
        """
        if type(kind) is str:
            assert kind in _METADATA_TYPES
            self.__kind = _METADATA_TYPES[kind]
        else:
            self.__kind = kind
        self.__name = name
        self._size = size
        self.__relative_prev_val = relative_prev_val
        self.__unique_values = []
        """Ordered values through the dimension"""

    @property
    def kind(self):
        return self.__kind

    def _setKind(self, kind):
        self.__kind = kind

    @property
    def name(self):
        return self.__name

    def _setName(self, name):
        self.__name = name

    @property
    def relative_prev_val(self):
        return self.__relative_prev_val

    def _set_relative_prev_val(self, value):
        self.__relative_prev_val = value

    @property
    def size(self):
        return self._size

    def _setSize(self, size):
        """
        .. note: having a setter was needed for GUI and Signal?SLOT stuff
                (see :class:`DimensionItem`)
        """
        self._size = size

    @property
    def unique_values(self):
        return self.__unique_values

    def _setUniqueValues(self, values):
        if len(values) != self.size:
            _logger.warning('Unique values set for %s, is incoherent with size' % self)
            raise ValueError('Unique values set for %s, is incoherent with size' % self)
        self.__unique_values = values

    def __str__(self):
        return " ".join((str(self.kind), str(self.name), 'size:', str(self.size)))

    def to_dict(self):
        """translate the current Dim to a dictionary"""
        return {
            'name': self.name,
            'kind': self.kind,
            'size': self.size,
            'relative_prev_val': self.relative_prev_val,
        }

    @staticmethod
    def from_dict(_dict):
        """

        :param dict _dict: dict defining the dimension. Should contains the
                           following keys: name, kind, size, relative_prev_val
                           unique values are not stored into it because it
                           depends on the metadata and should be obtain from a
                           fit / set_dims
        :return: Dim corresponding to the dict given
        :rtype: :class:`Dim`
        """
        assert type(_dict) is dict
        missing_keys = []
        for _key in ('name', 'kind', 'size', 'relative_prev_val'):
            if _key not in _dict:
                missing_keys.append(missing_keys)
        if len(missing_keys) > 0:
            raise ValueError('There is some missing keys (%s), unable to create'
                             'a valid Dim')
        else:
            return Dim(name=_dict['name'],
                       kind=_dict['kind'],
                       size=_dict['size'],
                       relative_prev_val=_dict['relative_prev_val'])
