# coding: utf-8
#/*##########################################################################
# Copyright (C) 2016 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
#############################################################################*/


__authors__ = ["H.Payno"]
__license__ = "MIT"
__date__ = "15/05/2017"

import os
import tempfile

import fabio
import numpy

from id06workflow.core.experiment import Dataset
from id06workflow.core.operation.mapping import _MappingBase


def createRandomDataset(dims, nb_data_files=10, nb_dark_files=2,
                        nb_ff_file=0, _dir=None):
    """Simple creation of a dataset in _dir with the requested number of data
    files, dark files and flat field files
    
    :param tuple of int dims: dimensions of the files
    """
    assert type(dims) is tuple and len(dims) is 2
    assert type(nb_data_files) is int
    assert type(nb_dark_files) is int
    assert type(nb_ff_file) is int
    assert type(_dir) in (None, str)
    if _dir is None:
        _dir = tempfile.mkdtemp()

    if os.path.isdir(_dir) is False:
        raise ValueError("%s is not a directory" % _dir)
    dataset = Dataset()
    for index in range(nb_data_files):
        data_file = os.path.join(_dir, 'data_file%04i.edf' % index)
        image = fabio.edfimage.EdfImage(data=numpy.random.random(dims))
        image.write(data_file)
    dataset.data_files_pattern = os.path.join(_dir, 'data_file%04i.edf' % 0)

    for index in range(nb_dark_files):
        dark_file = os.path.join(_dir, 'dark_file%04i.edf' % index)
        image = fabio.edfimage.EdfImage(data=numpy.random.random(dims))
        image.write(dark_file)
        dataset.addDarkFile(dark_file)

    for index in range(nb_ff_file):
        ff_file = os.path.join(_dir, 'flat_field_file%04i.edf' % index)
        image = fabio.edfimage.EdfImage(data=numpy.random.random(dims))
        image.write(ff_file)
        dataset.addFlatFieldFile(ff_file)

    return dataset


def createDataset(pattern, background, dx=0, dy=0, dz=0, nb_data_files=10,
                  nb_flat_field_files=0, _dir=None):
    """
    Create a dataset from a configuration

    :param tuple of int dims: image dims
    :param numpy.ndarray pattern: pattern of the image
    :param numpy.ndarray background: noise to add to the image (detector noise)
    :param float dx: x translation between each image
    :param float dy: y translation between each image
    :param float dz: z translation between each image
    :param int nb_data_files:
    :param str or None _dir:

    :return :class:`Dataset`: generated instance of :class:`Dataset`
    """
    assert type(nb_data_files) is int
    assert type(_dir) in (None, str)
    if dx != 0.0 or dy != 0.0 or dz != 0.0:
        raise ValueError('Translation between images is not managed yet')

    if _dir is None:
        _dir = tempfile.mkdtemp()

    if os.path.isdir(_dir) is False:
        raise ValueError("%s is not a directory" % _dir)
    dataset = Dataset()
    for index in range(nb_data_files):
        data_file = os.path.join(_dir, 'data_file%04i.edf' % index)
        image = fabio.edfimage.EdfImage(data=pattern+background)
        image.write(data_file)
    if nb_data_files > 0:
        dataset.data_files_pattern = os.path.join(_dir, 'data_file%04i.edf' % 0)

    for index in range(nb_flat_field_files):
        ff_file = os.path.join(_dir, 'flat_field_file%04i.edf' % index)
        image = fabio.edfimage.EdfImage(data=background)
        image.write(ff_file)
        dataset.addFlatFieldFile(ff_file)

    return dataset


def createRandomMap(shape, ndim=1):
    """
    Create a dummy map which can be used for displaying some test

    :param tuple shape: shape of the maps
    :param int ndim: 
    """
    return _DummyMap(shape=shape, ndim=ndim)


class _DummyMap(_MappingBase):
    def __init__(self, shape, ndim):
        _MappingBase.__init__(self, None, name='Dummy map')
        self.__dim = []
        for iDim in range(ndim):
            self.__dim.append(numpy.zeros((*(shape), 4)))
            for imap in range(4):
                self.dims[iDim][:, :, imap] = numpy.random.random(shape)
        self.__intensity_map = numpy.random.random(shape)

    @property
    def dims(self):
        return self.__dim

    @property
    def intensity_map(self):
        return self.__intensity_map

    @property
    def ndim(self):
        return len(self.__dim)
