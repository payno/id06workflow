# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "05/10/2018"


from collections import namedtuple
from silx.gui import qt
from silx.gui.plot.StackView import StackViewMainWindow

from id06workflow.core.experiment import Experiment
from id06workflow.core.operation import noisereduction
from id06workflow.gui.settings import DEFAULT_COLORMAP
from id06workflow.gui.utils import _IllustrationWidget

_ReconsParams = namedtuple('_ReconsParam', ['scale'])


class NoiseReductionWidget(qt.QWidget):
    """
    Widget used to define and apply a noise reduction
    """
    def __init__(self, parent):
        qt.QWidget.__init__(self, parent)
        # Here a lot of better stuff should be done, just two plots and
        # synchronized sliders
        # TODO: maybe the result and the noise reduction parameters can be
        # displayed into separate windows.
        self._cache_data = None
        """Cache data used because apply correction thought several operation"""
        self._params_has_changed = False
        """Flag to record if the current display fit the latest modifications"""
        self._experiment = None

        self.setLayout(qt.QVBoxLayout())
        self._display = _DisplayNoiseReduction(parent=self)
        self.layout().addWidget(self._display)
        self._paramsWidget = _NoiseReductionTable(parent=self)
        self.layout().addWidget(self._paramsWidget)

        # signal/SLOT connections
        self._paramsWidget.sigParamsChanged.connect(self._paramChanged)

        # expose API
        self.setBackgroundSubtraction = self._paramsWidget.setBackgroundSubtraction
        self.setLowerThreshold = self._paramsWidget.setLowerThreshold
        self.setUpperThreshold = self._paramsWidget.setUpperThreshold

        # operation
        self._lowerThreshold = None
        self._upperThreshold = None
        self._backgroundCorrection = None
        self._maskCorrection = None

    def _updateCorrection(self):
        self._cache_data = self._experiment.data_flatten[...]

        if self._params_has_changed is True:
            self._params_has_changed = False

            if self._paramsWidget.isMaskCorrectionActive() is True:
                mask_type = self._paramsWidget.getMaskCorrectionType()
                assert mask_type in noisereduction.MaskSubstraction.MASK_TYPES
                params = self._paramsWidget.getMaskCorrectionParameters()
                assert params is not None
                self._maskCorrection.mask_type = mask_type
                self._maskCorrection.scale = params.scale
                self._cache_data = self._maskCorrection.dry_run(self._cache_data)

            if self._paramsWidget.isBackgroundSubsActive() is True:
                self._cache_data = self._backgroundCorrection.dry_run(self._cache_data)

            if self._paramsWidget.isThreshodSubsActive() is True:
                threshold = self._paramsWidget.getUpperThreshold()
                if threshold is not None:
                    self._upperThreshold.threshold = threshold
                    self._upperThreshold.replacement_value = 0.0
                    self._cache_data = self._upperThreshold.dry_run(self._cache_data)
                threshold = self._paramsWidget.getLowerThreshold()
                if threshold is not None:
                    self._lowerThreshold.threshold = threshold
                    self._cache_data = self._lowerThreshold.dry_run(self._cache_data)

            self._display.set(self._experiment.data_flatten,
                              self._cache_data)

    def validate_correction(self):
        """
        Apply all the dry_run to keep the current operations
        """
        if self._paramsWidget.isBackgroundSubsActive() is True:
            self._backgroundCorrection.apply()
        if self._paramsWidget.isThreshodSubsActive() is True:
            threshold = self._paramsWidget.getUpperThreshold()
            if threshold is not None:
                self._upperThreshold.apply()
            threshold = self._paramsWidget.getLowerThreshold()
            if threshold is not None:
                self._lowerThreshold.apply()

        if self._paramsWidget.isMaskCorrectionActive() is True:
            self._maskCorrection.apply()

    def reset(self, experiment):
        assert isinstance(experiment, Experiment)
        self._experiment = experiment
        self._lowerThreshold = noisereduction.ThresholdRemoval(experiment=experiment, lower=True)
        self._upperThreshold = noisereduction.ThresholdRemoval(experiment=experiment, lower=False)
        self._backgroundCorrection = noisereduction.BackgroundSubtraction(experiment=experiment)
        self._maskCorrection = noisereduction.MaskSubstraction(experiment=experiment)

        self._params_has_changed = True
        self._updateCorrection()

    def _paramChanged(self):
        """Callback when noise reduction is changed"""
        self._params_has_changed = True

    def clearExperiment(self):
        self._experiment = None
        self._display.set(None, None)
        self._lowerThreshold = None
        self._upperThreshold = None
        self._backgroundCorrection = None
        self._maskCorrection = None


class _DisplayNoiseReduction(qt.QWidget):
    """
    Widget used to display raw data along data with noise reduction
    """
    def __init__(self, parent):
        qt.QWidget.__init__(self, parent)
        self.setLayout(qt.QHBoxLayout())
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.layout().setSpacing(0)
        self._rawDataView = StackViewMainWindow()
        self.layout().addWidget(self._rawDataView)
        self._treatedDataView = StackViewMainWindow()
        self.layout().addWidget(self._treatedDataView)
        # TODO: those two stack plot should be synchronized (control, colormap...)

        # connect colormap
        self._rawDataView.getPlot().setDefaultColormap(DEFAULT_COLORMAP)
        self._treatedDataView.getPlot().setDefaultColormap(DEFAULT_COLORMAP)

    def set(self, data, corrected_data):
        if data is not None:
            self._rawDataView.setStack(data)
        else:
            self._rawDataView.clear()
        if corrected_data is not None:
            self._treatedDataView.setStack(corrected_data)
        else:
            self._treatedDataView.clear()


class _NoiseReductionTable(qt.QTabWidget):
    """
    TODO: maybe this can be a widget by itself...
    """
    sigParamsChanged = qt.Signal()
    """Signal emitted when the noise reduction parameters changed"""

    def __init__(self, parent):
        qt.QTabWidget.__init__(self, parent)
        self._maskWidget = _MaskParamsWidget(parent=self)
        self.addTab(self._maskWidget, 'mask')
        self._backgroundWidget = _BackgroundWidget(parent=self)
        self.addTab(self._backgroundWidget, 'background substraction')
        self._thresholdWidget = _ThresholdWidget(parent=self)
        self.addTab(self._thresholdWidget, 'threshold removal')
        # TODO: mask, threshod and backgournd substraction can be managed in
        # dedicated process. Not sure this is interesting

        # signal/SLOT connections
        self._maskWidget.sigParamsChanged.connect(self._paramsChanged)
        self._backgroundWidget.sigChanged.connect(self._paramsChanged)
        self._thresholdWidget.sigChanged.connect(self._paramsChanged)

        # expose API
        self.isMaskCorrectionActive = self._maskWidget.isCorrectionActive
        self.getMaskCorrectionParameters = self._maskWidget.getCorrectionParameters
        self.getMaskCorrectionType = self._maskWidget.getCorrectionType
        self.isBackgroundSubsActive = self._backgroundWidget.isBackgroundSubsActive
        self.isThreshodSubsActive = self._thresholdWidget.isThreshodSubsActive
        self.getLowerThreshold = self._thresholdWidget.getLowerThreshold
        self.getUpperThreshold = self._thresholdWidget.getUpperThreshold

        self.setBackgroundSubtraction = self._backgroundWidget.setBackgroundSubtraction
        self.setLowerThreshold = self._thresholdWidget.setLowerThreshold
        self.setUpperThreshold = self._thresholdWidget.setUpperThreshold

    def _paramsChanged(self):
        self.sigParamsChanged.emit()


class _BackgroundWidget(qt.QWidget):
    sigChanged = qt.Signal()
    """Signal emitted when the background option changed"""

    def __init__(self, parent):
        qt.QWidget.__init__(self, parent)
        self.setLayout(qt.QHBoxLayout())
        self._backgroundQCB = qt.QCheckBox('background', parent=self)
        self._backgroundQCB.toggled.connect(self._changed)

    def isBackgroundSubsActive(self):
        return self._backgroundQCB.isChecked() is True

    def _changed(self, *args, **kwargs):
        self.sigChanged.emit()

    def setBackgroundSubtraction(self, value):
        self._backgroundQCB.setChecked(value)


class _ThresholdWidget(qt.QWidget):
    sigChanged = qt.Signal()
    """Signal emitted when the background option changed"""
    class ThresholdWidget(qt.QWidget):
        sigChanged = qt.Signal()
        def __init__(self, parent, name, value):
            qt.QWidget.__init__(self, parent)
            self.setLayout(qt.QHBoxLayout())
            self.layout().setContentsMargins(0, 0, 0, 0)
            self.layout().setSpacing(0)
            self._qcb = qt.QCheckBox(name, parent=self)
            self.layout().addWidget(self._qcb)
            self._threshold = qt.QLineEdit(str(value), parent=self)
            self._threshold.setValidator(qt.QDoubleValidator(self._threshold))
            self.layout().addWidget(self._threshold)

            self._threshold.setEnabled(False)

            # connect Signal/SLOT
            self._threshold.textChanged.connect(self._changed)
            self._qcb.toggled.connect(self._changed)
            self._qcb.toggled.connect(self._threshold.setEnabled)

        def _changed(self, *args, **kwargs):
            self.sigChanged.emit()

        def isActive(self):
            return self._qcb.isChecked()

        def getValue(self):
            if self._qcb.isChecked() is False:
                return None
            else:
                return float(self._threshold.text())

        def setThreshold(self, value):
            """

            :param float, int or None value: value to give to the threshold
            """
            self._qcb.setChecked(value is not None)
            if value is not None:
                self._threshold.setText(str(value))

    def __init__(self, parent):
        qt.QWidget.__init__(self, parent)
        self.setLayout(qt.QVBoxLayout())

        self._lowerThreshold = self.ThresholdWidget(parent=self,
                                                    name='lower threshold',
                                                    value=0.0)
        self.layout().addWidget(self._lowerThreshold)

        self._upperThreshold = self.ThresholdWidget(parent=self,
                                                    name='upper threshold',
                                                    value=5000)
        self.layout().addWidget(self._upperThreshold)

        # expose API
        self.getLowerThreshold = self._lowerThreshold.getValue
        self.getUpperThreshold = self._upperThreshold.getValue
        self.setLowerThreshold = self._lowerThreshold.setThreshold
        self.setUpperThreshold = self._upperThreshold.setThreshold

        # connect signal / SLOTS
        self._lowerThreshold.sigChanged.connect(self._changed)
        self._upperThreshold.sigChanged.connect(self._changed)

    def isThreshodSubsActive(self):
        return self._upperThreshold.isActive() or self._lowerThreshold.isActive()

    def _changed(self, *args, **kwargs):
        self.sigChanged.emit()


class _MaskParamsWidget(qt.QWidget):
    """
    Widget used to defined the parameters to be applied to the noise reduction
    """
    sigParamsChanged = qt.Signal()
    """Signal emitted when the noise reduction parameters changed"""

    def __init__(self, parent):
        qt.QWidget.__init__(self, parent)
        self.setLayout(qt.QHBoxLayout())
        self._maskGB = _MaskGroupBox(parent=self)
        self.layout().addWidget(self._maskGB)
        self._illustration = _IllustrationWidget(parent=self, img='todo')
        self.layout().addWidget(self._illustration)

        # signal/SLOT connections
        self._maskGB.sigMaskChanged.connect(self._paramsChanged)

        # expose API
        self.isCorrectionActive = self._maskGB.isCorrectionActive
        self.getCorrectionParameters = self._maskGB.getCorrectionParameters
        self.getCorrectionType = self._maskGB.getCorrectionType

    def _paramsChanged(self):
        self.sigParamsChanged.emit()


class _MaskGroupBox(qt.QGroupBox):
    sigMaskChanged = qt.Signal()
    """Signal emitted when the mask definition change"""

    def __init__(self, parent):
        qt.QGroupBox.__init__(self, 'mask', parent)
        self.setCheckable(True)

        self.setLayout(qt.QVBoxLayout())
        self._maskTypeCB = qt.QComboBox(parent=self)
        for _type in noisereduction.MaskSubstraction.MASK_TYPES:
            self._maskTypeCB.addItem(_type)
        self.layout().addWidget(self._maskTypeCB)

        self._radWidget = qt.QWidget(parent=self)
        self._radWidget.setLayout(qt.QHBoxLayout())
        self._radWidget.layout().setContentsMargins(0, 0, 0, 0)
        self._radWidget.layout().setSpacing(0)
        self._radWidget.layout().addWidget(qt.QLabel('scale:', self))
        self._radValue = qt.QLineEdit('1.0', parent=self)
        validator = qt.QDoubleValidator(self._radValue)
        validator.setBottom(0.0)
        self._radValue.setValidator(validator)
        self._radWidget.layout().addWidget(self._radValue)
        self.layout().addWidget(self._radWidget)

        # signal/SLOT connections
        self.toggled.connect(self._maskChanged)
        self._maskTypeCB.currentIndexChanged.connect(self._maskChanged)
        self._radValue.textChanged.connect(self._maskChanged)

    def isCorrectionActive(self):
        return self.isChecked()

    def getCorrectionType(self):
        """
        
        :return str or None: the type of mask to apply 
        """
        if self.isChecked() is False:
            return None
        else:
            return self._maskTypeCB.currentText()

    def getCorrectionParameters(self):
        """
        
        :return float or None: scale to apply to the data
        """
        if self.isChecked() is False:
            return None
        else:
            return _ReconsParams(scale=float(self._radValue.text()))

    def _maskChanged(self, *args, **kwargs):
        self.sigMaskChanged.emit()
