# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "04/10/2018"


from silx.gui import qt
from collections import OrderedDict
from id06workflow.core.experiment import Dataset
import os
import logging
_logger = logging.getLogger(__file__)


class DatasetSelectionDialog(qt.QDialog):
    """
    dialog used to validate the dataset selection
    """
    def __init__(self, parent):
        qt.QDialog.__init__(self, parent)
        self.setWindowTitle('Dataset definition')
        self.setWindowFlags(qt.Qt.Widget)
        types = qt.QDialogButtonBox.Ok | qt.QDialogButtonBox.Cancel
        _buttons = qt.QDialogButtonBox(parent=self)
        _buttons.setStandardButtons(types)
        self.mainWindow = DatasetSelection(parent=self)

        self.setLayout(qt.QVBoxLayout())
        self.layout().addWidget(self.mainWindow)
        self.layout().addWidget(_buttons)

        _buttons.accepted.connect(self.accept)
        _buttons.rejected.connect(self.reject)

        # expose API
        self.getDataFilesPattern = self.mainWindow.getDataFilesPattern
        self.getDarkFiles = self.mainWindow.getDarkFiles
        self.getFlatFieldFiles = self.mainWindow.getFlatFieldFiles


class DatasetSelection(qt.QTabWidget):
    """
    Main widget used to define the dta files, dark files and flat field files
    """
    def __init__(self, parent):
        qt.QTabWidget.__init__(self, parent)
        self._dataFiles = FilePatternWidget(parent=self)
        self.addTab(self._dataFiles, 'data files pattern:')
        self._darkFiles = DataListWidget(parent=self)
        self.addTab(self._darkFiles, 'dark files')
        self._ffFiles = DataListWidget(parent=self)
        self.addTab(self._ffFiles, 'flat field files')

        # expose API
        self.getDataFilesPattern = self._dataFiles.getPattern
        self.setDataFilesPattern = self._dataFiles.setPattern
        self.getDarkFiles = self._darkFiles.getFiles
        self.setDarkFiles = self._darkFiles.setFiles
        self.addDarkFile = self._darkFiles.addFile
        self.getFlatFieldFiles = self._ffFiles.getFiles
        self.setFlatFieldFiles = self._ffFiles.setFiles
        self.addFlatFieldFile = self._ffFiles.addFile

    def getDataset(self):
        """
        
        :return: dataset defined on the GUI
        :rtype: :class:`Dataset`
        """
        return Dataset(data_files_pattern=self.getDataFilesPattern(),
                       dark_files=self.getDarkFiles(),
                       ff_files=self.getFlatFieldFiles())

    def setDataset(self, dataset):
        """
        Update the GUI to display the values of the given dataset

        :param :class:`Dataset` dataset: 
        """
        assert isinstance(dataset, Dataset)
        self._dataFiles.setPattern(dataset.data_files_pattern)
        self._darkFiles.setFiles(dataset.dark_files)
        self._ffFiles.setFiles(dataset._ff_files)


class FilePatternWidget(qt.QWidget):
    """A simple interface to select files from a pattern

    .. warning: the widget won't check for scan validity and will only
        emit the path to folders to the next widgets

    :param parent: the parent widget
    """
    def __init__(self, parent=None):
        qt.QWidget.__init__(self, parent)
        self.setLayout(qt.QHBoxLayout())
        self.layout().addWidget(qt.QLabel('file pattern:', parent=self))
        self._file_pattern = qt.QLineEdit('', parent=self)
        self.layout().addWidget(self._file_pattern)

    def getPattern(self):
        return str(self._file_pattern.text())

    def setPattern(self, pattern):
        return self._file_pattern.setText(str(pattern))


class DataListWidget(qt.QWidget):
    """A simple list of dataset path.

    .. warning: the widget won't check for scan validity and will only
        emit the path to folders to the next widgets

    :param parent: the parent widget
    """

    def __init__(self, parent=None):
        qt.QWidget.__init__(self, parent)

        self.setLayout(qt.QVBoxLayout())
        self.datalist = FileList(parent=self)
        self.layout().addWidget(self.datalist)
        self.layout().addWidget(self.__getAddAndRmButtons())

        # expose API
        self.getFiles = self.datalist.getFiles
        self.setFiles = self.datalist.setFiles
        self.addFile = self.datalist.add

    def __getAddAndRmButtons(self):
        lLayout = qt.QHBoxLayout()
        w = qt.QWidget(self)
        w.setLayout(lLayout)
        self._addButton = qt.QPushButton("Add")
        self._addButton.clicked.connect(self._callbackAddFiles)
        self._rmButton = qt.QPushButton("Remove")
        self._rmButton.clicked.connect(self._callbackRemoveFiles)

        spacer = qt.QWidget(self)
        spacer.setSizePolicy(qt.QSizePolicy.Expanding,
                             qt.QSizePolicy.Minimum)
        lLayout.addWidget(spacer)
        lLayout.addWidget(self._addButton)
        lLayout.addWidget(self._rmButton)

        return w

    def _callbackAddFiles(self):
        """"""
        dialog = qt.QFileDialog(self)
        # dialog.setNameFilters(["ini (*.ini)"])
        dialog.setFileMode(qt.QFileDialog.ExistingFile)

        if not dialog.exec_():
            dialog.close()
            return

        foldersSelected = dialog.selectedFiles()
        for folder in foldersSelected:
            assert (os.path.isfile(folder))
            self.datalist.add(folder)

    def _callbackRemoveFiles(self):
        """"""
        selectedItems = self.datalist.selectedItems()
        if selectedItems is not None:
            for item in selectedItems:
                # self.datalist.remove(item)
                self.datalist.remove_item(item)


class FileList(qt.QTableWidget):
    """
    Simple GUI used to define a list of file
    """
    def __init__(self, parent):
        qt.QTableWidget.__init__(self, parent)
        self._selection = set()

        self.setRowCount(0)
        self.setColumnCount(1)
        self.setSortingEnabled(True)
        self.setHorizontalHeaderLabels(['file'])
        self.verticalHeader().hide()
        if hasattr(self.horizontalHeader(), 'setSectionResizeMode'):  # Qt5
            self.horizontalHeader().setSectionResizeMode(0,
                                                         qt.QHeaderView.Stretch)
        else:  # Qt4
            self.horizontalHeader().setResizeMode(0, qt.QHeaderView.Stretch)
        self.setAcceptDrops(True)
        self.items = OrderedDict()

    def getFiles(self):
        return self._selection

    def setFiles(self, _files):
        if _files is None:
            return
        self.clear()
        for _file in _files:
            self.add(_file)

    def remove_item(self, item):
        """
        Remove a given item to the Table
        """
        del self.items[item.text()]
        itemIndex = self.row(item)
        self.takeItem(itemIndex, 0)
        self._selection.remove(item.text())
        self.removeRow(item.row())
        self.setRowCount(self.rowCount() - 1)
        self._update()

    def remove(self, scan):
        if scan is not None and scan in self.items:
            item = self.items[scan]
            itemIndex = self.row(item)
            self.takeItem(itemIndex, 0)
            self.remove(self, scan)
            del self.items[scan]
            self.removeRow(item.row())
            self.setRowCount(self.rowCount() - 1)
            self._update()

    def _update(self):
        list_scan = list(self.items.keys())
        self.clear()
        for scan in list_scan:
            self.add(scan)
        self.sortByColumn(0, self.horizontalHeader().sortIndicatorOrder())

    def _addScanIDItem(self, d):
        if(not os.path.isfile(d)):
            warning = "Skipping the observation of %s, directory not existing on the system" % d
            _logger.info(warning)
        elif d in self.items:
            warning = "The directory %s is already in the scan list" % d
            _logger.info(warning)
        else:
            row = self.rowCount()
            self.setRowCount(row + 1)

            _item = qt.QTableWidgetItem()
            _item.setText(d)
            _item.setFlags(qt.Qt.ItemIsEnabled | qt.Qt.ItemIsSelectable)
            self.setItem(row, 0, _item)

            self.items[d] = _item

    def add(self, _file):
        """Add the file _file to the list

        :param _file: the path of the file to add
        :type _file: str
        """
        if os.path.isfile(_file):
            self._addScanIDItem(_file)
            self._selection.add(_file)

    def clear(self):
        """Remove all items on the list"""
        self.items = OrderedDict()
        self._selection.clear()
        qt.QTableWidget.clear(self)
        self.setRowCount(0)
        self.setHorizontalHeaderLabels(['file'])
        if hasattr(self.horizontalHeader(), 'setSectionResizeMode'):  # Qt5
            self.horizontalHeader().setSectionResizeMode(0,
                                                         qt.QHeaderView.Stretch)
        else:  # Qt4
            self.horizontalHeader().setResizeMode(0, qt.QHeaderView.Stretch)

    def dropEvent(self, event):
        if event.mimeData().hasFormat('text/uri-list'):
            for url in event.mimeData().urls():
                self.add(str(url.path()))

    def supportedDropActions(self):
        """Inherited method to redefine supported drop actions."""
        return qt.Qt.CopyAction | qt.Qt.MoveAction

    def dragEnterEvent(self, event):
        if event.mimeData().hasFormat('text/uri-list'):
            event.accept()
            event.setDropAction(qt.Qt.CopyAction)
        else:
            qt.QListWidget.dragEnterEvent(self, event)

    def dragMoveEvent(self, event):
        if event.mimeData().hasFormat('text/uri-list'):
            event.setDropAction(qt.Qt.CopyAction)
            event.accept()
        else:
            qt.QListWidget.dragMoveEvent(self, event)


def main():
    app = qt.QApplication([])
    s = DatasetSelection(parent=None)
    s.show()
    app.exec_()


if __name__ == "__main__":
    main()
