# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "03/10/2018"


import unittest
from id06workflow.gui.dimensions import DimensionMapping
from silx.gui import qt



# TODO: look, not orking with the TestCaseQt, has unreleased widgets
class TestDimensionMapping(unittest.TestCase):
    """
    Make sure the :class:`DimensionMapping` is correctly adding and removing
    dimension items
    """
    def setUp(self):
        unittest.TestCase.setUp(self)
        self._app = qt.QApplication.instance() or qt.QApplication([])
        self.widget = DimensionMapping(parent=None)

    def tearDown(self):
        self.widget.setAttribute(qt.Qt.WA_DeleteOnClose)
        self.widget.close()
        unittest.TestCase.tearDown(self)

    def testAddRmItems(self):
        """
        Make sure adding and removing dimension items are correct
        """
        dim1 = self.widget.addDim()
        dim2 = self.widget.addDim()
        self.assertTrue(self.widget.ndim is 2)
        self.assertTrue(dim1.axis is 0)
        self.assertTrue(dim2.axis is 1)
        dim1.axis = 4
        self.assertTrue(dim1.axis is 4)
        self.widget.removeDim(dim1)
        self.assertTrue(self.widget.ndim is 1)
        dim2.remove()
        self.assertTrue(self.widget.ndim is 0)
        self.widget.addDim()
        self.widget.addDim()
        self.assertTrue(self.widget.ndim is 2)
        self.widget.clear()
        self.assertTrue(self.widget.ndim is 0)


def suite():
    test_suite = unittest.TestSuite()
    for ui in (TestDimensionMapping, ):
        test_suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(ui))
    return test_suite


if __name__ == '__main__':
    unittest.main(defaultTest="suite")
