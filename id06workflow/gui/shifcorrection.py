# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "08/10/2018"


import logging
from functools import partial
from silx.gui import qt
from silx.gui.plot import Plot2D
from silx.gui.plot.StackView import StackViewMainWindow

from id06workflow.core.operation.ThreadedOperation import \
    ThreadedOperation

from id06workflow.core.image import guess_shift
from id06workflow.core.operation.shift import Shift as ShiftOperation
from id06workflow.gui.settings import DEFAULT_COLORMAP

_logger = logging.getLogger(__file__)


class ShiftCorrectionWidget(qt.QWidget):
    """
    Used to apply a shift correction on data
    """
    sigProgress = qt.Signal(float)
    """Advancement of the applied shift in percentage"""

    def __init__(self, parent):
        self._experiment = None
        self._operation_thread = ThreadedOperation()

        qt.QWidget.__init__(self, parent)
        self.setLayout(qt.QVBoxLayout())

        self._control = _DxDyDzWidget(parent=self)
        self.layout().addWidget(self._control)
        self._displayTab = qt.QTabWidget(parent=self)
        self.layout().addWidget(self._displayTab)

        self._stack = StackViewMainWindow()
        self._stack.getPlot().setDefaultColormap(DEFAULT_COLORMAP.copy())
        self._displayTab.addTab(self._stack, 'images')

        self._sumPlot = Plot2D(parent=self)
        self._sumPlot.setDefaultColormap(DEFAULT_COLORMAP.copy())
        self._displayTab.addTab(self._sumPlot, 'images sum')

        self._rawData = None
        self._lastShift = (0, 0, 0)

        # expose API
        self.getDx = self._control.getDx
        self.getDy = self._control.getDy
        self.getDz = self._control.getDz
        self.getShift = self._control.getShift
        self.isProcessing = self._operation_thread.isRunning

        # Signal?slot estimation
        self._control._estimate_button.pressed.connect(self._make_estimation)

    def setShift(self, dx, dy, dz):
        self._control.setShift(dx=dx, dy=dy, dz=dz)
        self._updateShift()

    def _updateShift(self):
        """Update stack plot view from the current shift"""
        # TODO: the call to shift volume should be done in an other thread
        # and show some information on advancement
        if self._operation_thread.isRunning() is True:
            _logger.error('Cannot process shift until the current one is finished')
            return

        if self._experiment is None:
            _logger.error(
                'Cannot process shift because no experiment has been set')
            return

        _shift = self.getShift()
        if _shift == self._lastShift:
            self.setProgress(100)
            return
        else:
            self.setProgress(0)
            self._operation.dx = _shift[0]
            self._operation.dy = _shift[1]
            self._operation.dz = _shift[2]
            self._operation_thread.init(operation=self._operation,
                                        dry_run=True)

            self._callback = partial(self._update_post_processing, _shift)
            self._operation_thread.finished.connect(self._callback)
            self._operation.sigProgress.connect(self.setProgress)
            self._operation_thread.start()

    def _update_post_processing(self, _shift):
        self._operation_thread.finished.disconnect(self._callback)
        assert self._operation._cache_data is not None
        self._stack.setStack(self._operation._cache_data)
        self._sumPlot.addImage(self._operation._cache_data.sum(axis=0))
        self._lastShift = _shift
        self.setProgress(100)

    def setExperiment(self, experiment):
        if self._operation_thread.isRunning() is True:
            _logger.error('Cannot process shift until the current one is finished')

        self._experiment = experiment
        self._operation = ShiftOperation(experiment=self._experiment)

        self._rawData = self._operation.data
        self._lastShift = (0, 0, 0)
        self._stack.setStack(self._operation.data_flatten)
        self._sumPlot.addImage(self._operation.data_flatten.sum(axis=0))
        self._updateShift()

    def getOperation(self):
        return self._operation

    def setProgress(self, progress):
        assert type(progress) is int
        self.sigProgress.emit(progress)

    def _make_estimation(self):
        if self._experiment is None:
            _logger.warning('No experiment set, unable to estimate fit')
        x_shift = guess_shift(data=self._experiment.data_flatten, axis=-1)
        y_shift = guess_shift(data=self._experiment.data_flatten, axis=-2)
        self._control.setShift(dx=x_shift, dy=y_shift)


class _DxDyDzWidget(qt.QWidget):
    """
    Simple widget to allow user to define dx, dy and dz values (floats).
    """
    def __init__(self, parent):
        qt.QWidget.__init__(self, parent)
        self.setLayout(qt.QGridLayout())

        self.layout().addWidget(qt.QLabel('dx:', self), 1, 0)
        self._dxLE = qt.QLineEdit('0.0', parent=self)
        self._dxLE.setValidator(qt.QDoubleValidator(self._dxLE))
        self.layout().addWidget(self._dxLE, 1, 1)

        self.layout().addWidget(qt.QLabel('dy:', self), 2, 0)
        self._dyLE = qt.QLineEdit('0.0', parent=self)
        self._dyLE.setValidator(qt.QDoubleValidator(self._dyLE))
        self.layout().addWidget(self._dyLE, 2, 1)

        self.layout().addWidget(qt.QLabel('dz:', self), 3, 0)
        self._dzLE = qt.QLineEdit('0.0', parent=self)
        self._dzLE.setEnabled(False)
        self._dzLE.setValidator(qt.QDoubleValidator(self._dzLE))
        self.layout().addWidget(self._dzLE, 3, 1)

        self._estimate_button = qt.QPushButton('estimate', parent=self)
        self.layout().addWidget(self._estimate_button, 0, 0, 1, 2)

    def getDx(self):
        """

        :return float: 
        """
        return float(self._dxLE.text())

    def getDy(self):
        """

        :return float: 
        """
        return float(self._dyLE.text())

    def getDz(self):
        """

        :return float: 
        """
        return float(self._dzLE.text())

    def getShift(self):
        """

        :return tuple: (dx, dy, dz) 
        """
        return (self.getDx(), self.getDy(), self.getDz())

    def setShift(self, dx=None, dy=None, dz=None):
        """

        :param None or float dx: shift translation in x (for now on the stack of image)
        :param None or float dy: shift translation in y (for now on the stack of image) 
        :param None or float dz: shift translation in z (for now on the stack of image)
        """
        if dx is not None:
            self._dxLE.setText(str(dx))
        if dy is not None:
            self._dyLE.setText(str(dy))
        if dz is not None:
            self._dzLE.setText(str(dz))
