# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "04/10/2018"


from silx.gui import qt
from silx.gui.plot.tools.roi import RegionOfInterestManager
from silx.gui.plot.tools.roi import RegionOfInterestTableWidget
from silx.gui.plot.items.roi import RectangleROI
from silx.gui.plot.StackView import StackViewMainWindow
from id06workflow.gui.settings import DEFAULT_COLORMAP
import numpy


class ROIOnStackView(qt.QWidget):
    def __init__(self, parent):
        qt.QWidget.__init__(self, parent)
        self.setLayout(qt.QVBoxLayout())
        # TODO: if data starts to be quiet large then we can load only part of
        # if. There is some code to do this on tomwer.
        self._stack = StackViewMainWindow()
        self._stack.getPlot().setDefaultColormap(DEFAULT_COLORMAP)
        self.layout().addWidget(self._stack)
        self._plot = self._stack.getPlot()

        # Create the object controlling the ROIs and set it up
        self._roiManager = RegionOfInterestManager(self._plot)
        self._roiManager.setColor('red')  # Set the color of ROI

        self._roiManager.sigRoiAdded.connect(self.updateAddedRegionOfInterest)

        # Add a rectangular region of interest
        self._roi = RectangleROI()
        self._roi.setGeometry(origin=(0, 0), size=(2, 2))
        self._roi.setLabel('Initial ROI')
        self._roiManager.addRoi(self._roi)

        # Create the table widget displaying
        self._roiTable = RegionOfInterestTableWidget()
        self._roiTable.setRegionOfInterestManager(self._roiManager)

        # Add the region of interest table and the buttons to a dock widget
        widget = qt.QWidget()
        layout = qt.QVBoxLayout()
        widget.setLayout(layout)
        layout.addWidget(self._roiTable)

        def roiDockVisibilityChanged(visible):
            """Handle change of visibility of the roi dock widget

            If dock becomes hidden, ROI interaction is stopped.
            """
            if not visible:
                self._roiManager.stop()

        dock = qt.QDockWidget('Image ROI')
        dock.setWidget(widget)
        dock.visibilityChanged.connect(roiDockVisibilityChanged)
        self._plot.addDockWidget(qt.Qt.RightDockWidgetArea, dock)

        # expose some API
        self.getStack = self._stack.getStack

    # Set the name of each created region of interest
    def updateAddedRegionOfInterest(self, roi):
        """Called for each added region of interest: set the name"""
        if roi.getLabel() == '':
            roi.setLabel('ROI %d' % len(self._roiManager.getRois()))

    def setStack(self, stack, resetroi=True):
        assert stack is not None
        self._stack.setStack(stack)
        if resetroi is True:
            self._roi.setGeometry(origin=(0, 0),
                                  size=stack.shape[-2:])
        else:
            self._clipROI()
        # activate ROI edition by default
        self._roi.setEditable(True)

    def setROI(self, origin, size):
        self._roi.setGeometry(origin=origin, size=size)
        self._clipROI()

    def getROI(self):
        return self._roi

    def _clipROI(self):
        """clip current roi"""
        assert type(self._roi) is RectangleROI
        # clip roi on the current stack
        st = self.getStack(copy=False, returnNumpyArray=True)
        if st is None:
            return
        else:
            currentStack, params = st
        # TODO to test and probably to redo
        origin = (numpy.clip(self._roi.getOrigin()[0], 0, currentStack.shape[-1]),
                  numpy.clip(self._roi.getOrigin()[1], 0, currentStack.shape[-2]))
        size = (numpy.clip(self._roi.getSize()[0], 0, currentStack.shape[-1]),
                numpy.clip(self._roi.getSize()[0], 0, currentStack.shape[-1]))
        self._roi.setGeometry(origin=origin, size=size)


if __name__ == '__main__':
    app = qt.QApplication([])
    widget = ROIOnStackView(parent=None)
    widget.show()
    app.exec_()
