# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "10/10/2018"


import functools
import logging
from collections import OrderedDict
from silx.gui import qt
from silx.gui.plot import Plot2D

from id06workflow.core.mapping import MEAN, VARIANCE, SKEWNESS, KURTOSIS, MODES, \
    INTENSITY
from id06workflow.core.operation.mapping import _MappingBase
from id06workflow.gui import icons
from id06workflow.gui.settings import DEFAULT_COLORMAP

_logger = logging.getLogger(__file__)


class MappingPlot(qt.QMainWindow):
    """
    Simple plot used to display one of the result of the mapping (mean,
    variance, skewness, kurtosis)
    """
    def __init__(self, parent=None):
        qt.QMainWindow.__init__(self, parent)
        self._operation = None
        self.setWindowFlags(qt.Qt.Widget)

        self._plot = Plot2D(parent=self)
        self._plot.setDefaultColormap(DEFAULT_COLORMAP.copy())
        self.setCentralWidget(self._plot)

        self._data = {
            MEAN: None,
            VARIANCE: None,
            SKEWNESS: None,
            KURTOSIS: None,
            INTENSITY: None
        }
        """Contains images to be displayed according to the """

        # deal with toolbar
        self._modesToolbar = qt.QToolBar(parent=self)
        _axis_widget = qt.QWidget(parent=self)
        _axis_widget.setLayout(qt.QHBoxLayout())
        self._dimCB = qt.QComboBox(parent=_axis_widget)
        _axis_widget.layout().addWidget(qt.QLabel('dims:', self))
        _axis_widget.layout().addWidget(self._dimCB)
        # self._modesToolbar.addWidget(_axis_widget)
        self._intensityAction = None
        self._meanAction = None
        self._varianceAction = None
        self._skenessAction = None
        self._kurtosisAction = None
        self._actions = OrderedDict([
            (INTENSITY, self._intensityAction),
            (MEAN, self._meanAction),
            (VARIANCE, self._varianceAction),
            (SKEWNESS, self._skenessAction),
            (KURTOSIS, self._kurtosisAction),
        ])

        for mode, action in self._actions.items():
            icon = icons.getQIcon(mode)
            action = qt.QAction(icon, mode, self)
            action.setCheckable(True)
            callback = functools.partial(self.setMode, mode)
            action.changed.connect(callback)
            self._modesToolbar.addAction(action)
            self._actions[mode] = action
            if mode == INTENSITY:
                self._modesToolbar.addWidget(_axis_widget)

        self._modesToolbar.setIconSize(qt.QSize(70, 70))
        self._plot.addToolBar(qt.Qt.LeftToolBarArea, self._modesToolbar)
        self.setMode(INTENSITY)

        # connect signal / SLOT
        self._dimCB.currentIndexChanged.connect(self._updatePlot)

    def setMode(self, mode):
        """
        Change the display mode
        
        :param str mode: mode to be displayed. Should be in (mean, variance,
                         skeness, kurtosis)
        """
        assert mode in MODES
        for action_mode, action in self._actions.items():
            action.blockSignals(True)
            action.setChecked(action_mode == mode)
            action.blockSignals(False)

        self._updatePlot()

    def _updatePlot(self):
        self._plot.clear()
        if self._operation is None:
            return
        mode = self._getMode()
        dim = self._getDim()
        if dim == '':
            return

        assert dim in self._operation.dims
        if mode == INTENSITY:
            data = self._operation.intensity_map
        else:
            assert hasattr(self._operation.dims[dim], mode)
            data = getattr(self._operation.dims[dim], mode)
        self._plot.addImage(data)

    def _getMode(self):
        for action_mode, action in self._actions.items():
            if action.isChecked():
                return action_mode
        return None

    def _getDim(self):
        """

        :return: dimension the user want to display
        :rtype: int or None
        """
        if self._dimCB.size() is 0 or self._dimCB.currentText() == "":
            return None
        else:
            return int(self._dimCB.currentText())

    def setOperation(self, operation):
        self._operation = operation

        if operation is not None and operation.ndim > 0:
            assert isinstance(operation, _MappingBase)
            self._updateAxis()
            self._updatePlot()

    def _updateAxis(self):
        self._dimCB.blockSignals(True)
        self._dimCB.clear()
        if self._operation is None:
            return
        current_dim = self._getDim()
        for dim in self._operation.dims:
            self._dimCB.addItem(str(dim))

        idx = self._dimCB.findText(current_dim)
        if idx >= 0:
            self._dimCB.setCurrentIndex(idx)
        self._dimCB.blockSignals(False)
        self._updatePlot()


if __name__ == '__main__':
    app = qt.QApplication([])
    widget = MappingPlot(parent=None)
    widget.show()
    app.exec_()
