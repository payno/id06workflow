# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "03/10/2018"


from silx.gui import qt
from id06workflow.core.geometry.TwoThetaGeometry import TwoThetaGeometry
from id06workflow.gui.utils import _IllustrationWidget
from id06workflow.core.utils.char import THETA_CHAR
import logging

_logger = logging.getLogger(__file__)


class GeometryMainWidget(qt.QWidget):
    """
    Main Widget able to define all the geometries types
    """
    def __init__(self, parent):
        qt.QWidget.__init__(self, parent)
        self.setLayout(qt.QVBoxLayout())
        self.setContentsMargins(0, 0, 0, 0)
        self._geometryType = qt.QComboBox(parent=self)
        self._geometryType.addItem('First setup type')
        self.layout().addWidget(self._geometryType)

        # illustration
        self._illustration = _IllustrationWidget(parent=self, img='setup')
        self._illustration.setSizePolicy(qt.QSizePolicy.Expanding,
                                         qt.QSizePolicy.Expanding)
        self._illustration.setContentsMargins(0, 0, 0, 0)
        self._illustration.layout().setSpacing(0)
        self._illustration.setMinimumSize(qt.QSize(500, 300))
        self.layout().addWidget(self._illustration)

        self._twoThetaGeo = TwoThetaGeometryWidget(parent=self)
        self.layout().addWidget(self._twoThetaGeo)

    def setSetupGeometry(self, geometry):
        # TODO: in the future will depend on the geometry type
        self._twoThetaGeo.setSetupGeometry(geometry)

    def getSetupGeometry(self):
        return self._twoThetaGeo.getSetupGeometry()


class GeometryBaseWidget(object):
    """
    Pure virtual class for Geometry widgets
    """
    def getSetupGeometry(self):
        raise NotImplementedError('Pure virtual class')

    def setSetupGeometry(self, geometry):
        raise NotImplementedError('Pure virtual class')


class TwoThetaGeometryWidget(TwoThetaGeometry, qt.QWidget):
    """
    Widget used to defined a TwoTheta experimental setup
    """
    class DimensionComboBox(qt.QComboBox):
        def __init__(self, parent, default=None, locked=None):
            qt.QComboBox.__init__(self, parent)
            for itemName in ('diffry', 'obpitch'):
                self.addItem(itemName)
            if default is not None:
                idx = self.findText(default)
                if idx >= 0:
                    self.setCurrentIndex(idx)
                else:
                    _logger.warning('requested default value %s is not recognized' % default)
            self.setEnabled(not locked)

    def __init__(self, parent):
        qt.QWidget.__init__(self, parent)
        self.setLayout(qt.QGridLayout())

        self.layout().addWidget(qt.QLabel('2' + THETA_CHAR + ' (in degree)'), 0, 0)
        self._twothetaLE = qt.QLineEdit('0.2', parent=self)
        self._twothetaLE.setValidator(qt.QDoubleValidator(self._twothetaLE))
        self.layout().addWidget(self._twothetaLE, 0, 1)

        # TODO: replace by a dimension line input
        self.layout().addWidget(qt.QLabel('xmag (in ???)'), 1, 0)
        self._xmagLE = qt.QLineEdit('1.0', parent=self)
        self._xmagLE.setValidator(qt.QDoubleValidator(self._xmagLE))
        self.layout().addWidget(self._xmagLE, 1, 1)

        self.layout().addWidget(qt.QLabel('x pixel size (in ???)'), 2, 0)
        self._xpixLE = qt.QLineEdit('1.0', parent=self)
        self._xpixLE.setValidator(qt.QDoubleValidator(self._xpixLE))
        self.layout().addWidget(self._xpixLE, 2, 1)

        self.layout().addWidget(qt.QLabel('y pixel size (in ???)'), 3, 0)
        self._ypixLE = qt.QLineEdit('1.0', parent=self)
        self._ypixLE.setValidator(qt.QDoubleValidator(self._ypixLE))
        self.layout().addWidget(self._ypixLE, 3, 1)

        self.layout().addWidget(qt.QLabel('orientation'), 4, 0)
        self._orientationCB = qt.QComboBox(parent=self)
        for orientation in TwoThetaGeometry.ORIENTATIONS:
            self._orientationCB.addItem(orientation)
        self.layout().addWidget(self._orientationCB, 4, 1)
        # TODO: user should be able to save the configuration of the geometry

    def getSetupGeometry(self):
        orientation = self._orientationCB.currentText()
        assert orientation in TwoThetaGeometry.ORIENTATIONS
        return TwoThetaGeometry(twotheta=self.getTwoTheta(),
                                xmag=self.getXMag(),
                                xpixelsize=self.getXPixelSize(),
                                ypixelsize=self.getYPixelSize(),
                                orientation=TwoThetaGeometry._ORIENTATION_MAPPING[orientation])

    def setSetupGeometry(self, geometry):
        assert isinstance(geometry, TwoThetaGeometry)
        self.setTwoTheta(geometry.twotheta)
        self.setXMag(geometry.xmag)
        self.setXPixelSize(geometry.xpixelsize)
        self.setYPixelSize(geometry.ypixelsize)
        self.setOrientation(geometry.orientation)

    def getTwoTheta(self):
        return float(self._twothetaLE.text())

    def setTwoTheta(self, twotheta):
        self._twothetaLE.setText(str(twotheta))

    def getXMag(self):
        return float(self._xmagLE.text())

    def setXMag(self, xmag):
        self._xmagLE.setText(str(xmag))

    def getXPixelSize(self):
        return float(self._xpixLE.text())

    def setXPixelSize(self, xsize):
        self._xpixLE.setText(str(xsize))

    def getYPixelSize(self):
        return float(self._ypixLE.text())

    def setYPixelSize(self, ysize):
        self._ypixLE.setText(str(ysize))

    def setOrientation(self, orientation):
        assert orientation in TwoThetaGeometry.ORIENTATIONS
        index = self._orientationCB.findText(orientation)
        assert index >= 0
        self._orientationCB.setCurrentIndex(index)
