# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "03/10/2018"


from silx.gui import qt
from id06workflow.core.experiment import Dim, Experiment
from id06workflow.core.experiment import _METADATA_TYPES, _METADATA_TYPES_I, DEFAULT_METADATA
import logging

_logger = logging.getLogger(__file__)


class DimensionMapping(qt.QWidget):
    """
    Widget used to define the number of dimension and with which values they are
    mapped
    """
    _V_HEADERS = ['Axis', 'Kind', 'Name', 'Size', 'Is relative', '', '']

    def __init__(self, parent):
        qt.QWidget.__init__(self, parent)
        self.setLayout(qt.QGridLayout())

        self._table = qt.QTableWidget(parent=self)
        self._table.setColumnCount(len(self._V_HEADERS))
        self._table.setHorizontalHeaderLabels(self._V_HEADERS)
        header = self._table.horizontalHeader()
        if qt.qVersion() < "5.0":
            setResizeMode = header.setResizeMode
        else:
            setResizeMode = header.setSectionResizeMode
        for iColumn in range(len(self._V_HEADERS)):
            if iColumn in (1, 2):
                setResizeMode(iColumn, qt.QHeaderView.Stretch)
            else:
                setResizeMode(iColumn, qt.QHeaderView.ResizeToContents)
        self._table.verticalHeader().hide()
        self._dims = {}

        self.layout().addWidget(self._table, 0, 0, 6, 6)
        self._addButton = qt.QPushButton('add', parent=self)
        self.layout().addWidget(self._addButton, 6, 5, 1, 1)

        # connect Signal/SLOT
        self._addButton.pressed.connect(self.addDim)

    def clear(self):
        widgets = list(self._dims.values())
        for widget in widgets:
            self.removeDim(widget)

    @property
    def ndim(self):
        return len(self._dims)

    @property
    def dims(self):
        return self._dims

    def addDim(self, axis=None, dim=None):
        """

        :param axis: which axis is defining this dimension
        :param :class:Dim dim: definition of the dimension to add
        """
        if axis is None:
            axis = self._getNextFreeAxis()
        row = self._table.rowCount()
        self._table.setRowCount(row + 1)
        widget = _DimensionItem(parent=self, table=self._table, row=row)
        widget.removed.connect(self.removeDim)
        if dim is not None:
            widget.setDim(dim)
        widget.axis = axis
        self._dims[row] = widget
        return widget

    def setDims(self, dims):
        """

        :param dict dims: axis as key and :class:`Dim` as value
        :return: 
        """
        self.clear()
        assert type(dims) is dict
        for axis, dim in dims.items():
            assert type(axis) is int
            assert isinstance(dim, Dim)
            self.addDim(axis=axis, dim=dim)

    def removeDim(self, row):
        """

        :param int or _DimensionItem row: row or item to remove
        """
        if isinstance(row, _DimensionItem):
            iRow = row._row
        else:
            iRow = row
        self._table.setRowCount(self._table.rowCount() - 1)
        self._table.removeRow(iRow)
        self._dims[iRow].removed.disconnect(self.removeDim)
        self._dims[iRow].setAttribute(qt.Qt.WA_DeleteOnClose)
        self._dims[iRow].close()
        del self._dims[iRow]
        ini_rows = sorted(list(self._dims.keys()))
        for row in ini_rows:
            if row <= iRow:
                continue
            widget = self._dims[row]
            new_row = row - 1
            assert new_row >= 0
            widget.embedInTable(table=self._table, row=new_row)
            self._dims[new_row] = widget
            del self._dims[row]

    def _getNextFreeAxis(self):
        """

        :return int: next unused axis
        """
        res = 0
        usedAxis = []
        [usedAxis.append(_dim.axis) for _dim in self._dims.values()]
        while res in usedAxis:
            res = res + 1
        return res


class DimensionWidget(DimensionMapping):
    """
    Widget to define dimensions and try to fit those with experiment dataset
    """

    fitSucceed = qt.Signal()
    """Signal emitted when the fit succeed"""
    fitFailed = qt.Signal()
    """Signal emitted when the fit fail"""

    def __init__(self, parent):
        DimensionMapping.__init__(self, parent)
        self.__experiment = None
        self._fitButton = qt.QPushButton('fit', parent=self)
        self.layout().addWidget(self._fitButton, 6, 0, 1, 1)

        # connect Signal/SLOT
        self._fitButton.pressed.connect(self.fit)

    @property
    def experiment(self):
        return self.__experiment

    def setExperiment(self, experiment):
        """
        
        :param experiment: the experiment for which we want to define the
                           dimensions.
        :type experiment: :class:`Experiment`
        """
        assert isinstance(experiment, Experiment)
        self.__experiment = experiment
        if len(self.experiment.metadata) > 0:
            for widget in self._dims.values():
                widget._setMetadata(self.experiment.metadata[0])

    def fit(self):
        """
        Fit current dimension size to the dataset
        
        :return : return status of the fit and fail reasonif any
        :rtype: (bool str or None)
        """
        if self.__experiment is None:
            _logger.warning('No experiment to be fitted')
            return
        if self.ndim is 0:
            _logger.warning('No Dim defined to fit experiment dataset')
            return
        try:
            self.__experiment.set_dims(self.dims)
        except Exception as e:
            self.fitFailed.emit()
            return False, e
        else:
            self.fitSucceed.emit()
            return True, None

    def addDim(self, axis=None, dim=None):
        """

        :param axis: which axis is defining this dimension
        :param :class:`Dim` dim: definition of the dimension to add
        """
        widget = super().addDim(axis, dim)
        if self.__experiment is not None and len(self.__experiment.metadata) > 0:
            widget._setMetadata(self.__experiment.metadata[0])
        return widget


class _DimensionItem(Dim, qt.QWidget):
    """Widget use to define a dimension"""
    removed = qt.Signal(qt.QObject)
    """Signal emitted when the Item should be removed"""

    dimValueChanged = qt.Signal()
    """Signal emitted when the dimension definition is changed"""

    axisChanged = qt.Signal(int, int)
    """Signal emitted when the axis value is changed: id (row), new_value_value
    """

    class _SizeWidget(qt.QWidget):
        valueChanged = qt.Signal(int)
        """Signal emitted when the value of the Spin box change but decorelated
        with the active state
        """

        def __init__(self, parent):
            qt.QWidget.__init__(self, parent)
            self.setLayout(qt.QVBoxLayout())
            self._sizeSP = qt.QSpinBox(parent=self)
            """_sizeSP will be used to define the size. Will be editable if the
            size editable (so if dimension is set from a relative parameter)
            """
            self.layout().addWidget(self._sizeSP)

            self.layout().setContentsMargins(0, 0, 0, 0)
            self._active = False
            self._sizeSP.hide()
            # expose API
            self.setMinimum = self._sizeSP.setMinimum

            # connect Signal/SLOT
            self._sizeSP.valueChanged.connect(self._valueHasChanged)

        def setValue(self, size, editable=True):
            """
            
            :param int or None size: the size of the dimension. If None, not
                                     define yet
            :param editable: the size will be editable if given by the use (so)
                             if is relative.
            """
            if size is None:
                self._sizeSP.hide()
                self._sizeLabel.hide()
            else:
                assert type(size) is int
                self._sizeSP.show()
                self._sizeSP.setValue(size)
                self._sizeSP.setEnabled(editable)

        def toggle(self, checked):
            self._active = checked
            if checked:
                self._sizeSP.show()
            else:
                self._sizeSP.hide()

        def _valueHasChanged(self, value):
            self.valueChanged.emit(value)


    def __init__(self, parent, table, row):
        """

        :param QTableWidget table: if has to be embed in a table the
                                           parent table
        :param int row: row position in the QTableWidget. Also used as ID
        """
        qt.QWidget.__init__(self, parent)
        Dim.__init__(self, kind=DEFAULT_METADATA, name='')

        self.__metadata = None

        # axis
        self._axis = qt.QSpinBox(parent=self)
        self._axis.setMinimum(0)
        # kind
        self._kindCB = qt.QComboBox(parent=self)
        for _kindName in _METADATA_TYPES:
            self._kindCB.addItem(_kindName)
        # name
        self._namesCB = qt.QComboBox(parent=self)
        # size
        self._sizeWidget = self._SizeWidget(parent=self)
        self._sizeWidget.setMinimum(0)
        # relative
        self._relativeCB = qt.QCheckBox(parent=self)
        # rm button
        style = qt.QApplication.style()
        icon = style.standardIcon(qt.QStyle.SP_BrowserStop)
        self._rmButton = qt.QPushButton(icon=icon, parent=self)
        icon = style.standardIcon(qt.QStyle.SP_FileDialogContentsView)
        self._infoButton = qt.QPushButton(icon=icon, parent=self)
        self._infoButton.hide()

        # connect Signal/slot
        self._rmButton.pressed.connect(self.remove)
        self._relativeCB.toggled.connect(self._sizeWidget.toggle)
        self._axis.valueChanged.connect(self._axisHasChanged)
        self._kindCB.currentIndexChanged.connect(self._dimHasChanged)
        self._sizeWidget.valueChanged.connect(self._dimHasChanged)
        self._namesCB.currentIndexChanged.connect(self._dimHasChanged)
        self._relativeCB.toggled.connect(self._dimHasChanged)
        self._kindCB.currentIndexChanged.connect(self._updateNames)
        self._infoButton.pressed.connect(self.showUniqueNames)

        # update values from `Dim` class
        self._kindCB.currentTextChanged.connect(self._setKind)
        self._namesCB.currentTextChanged.connect(self._setName)
        self._sizeWidget.valueChanged.connect(self._setSize)
        self._relativeCB.toggled.connect(self._set_relative_prev_val)

        self.embedInTable(table=table, row=row)
        self.__row = row

    def _axisHasChanged(self, value):
        self.axisChanged.emit(self._row, value)

    def _dimHasChanged(self, *args, **kwargs):
        self.dimValueChanged.emit()

    def remove(self):
        self.removed.emit(self)

    def showUniqueNames(self):
        title = "%s - %s unique names" % (_METADATA_TYPES_I[self.kind], self.name)
        unique_values_str = []
        [unique_values_str.append(str(val)) for val in self.unique_values]
        qt.QMessageBox.information(self, title, ', '.join(unique_values_str))

    def _setUniqueValues(self, values):
        super()._setUniqueValues(values)
        if values is None:
            self._infoButton.hide()
        else:
            self._infoButton.show()

    def setDim(self, dim):
        assert isinstance(dim, Dim)
        _kind = _METADATA_TYPES_I[dim.kind]
        idx = self._kindCB.findText(_kind)
        assert idx >= 0
        self._kindCB.setCurrentIndex(idx)
        idx = self._namesCB.findText(dim.name)
        if idx >= 0:
            self._namesCB.setCurrentIndex(idx)
        else:
            self._namesCB.addItem(dim.name)
        if dim.size is not None:
            self._sizeWidget.setValue(dim.size)
        self._relativeCB.setChecked(dim.relative_prev_val)

    @property
    def _row(self):
        return self.__row

    @property
    def axis(self):
        return self._axis.value()

    @axis.setter
    def axis(self, axis):
        assert type(axis) is int
        self._axis.setValue(axis)

    @property
    def kind(self):
        assert self._kindCB.currentText() in _METADATA_TYPES
        return _METADATA_TYPES[self._kindCB.currentText()]

    @property
    def dim(self):
        return Dim(name=self.name, kind=self.kind, size=self.dimsize,
                   relative_prev_val=self.relative)

    @property
    def dimsize(self):
        if self.relative is True:
            return self._sizeWidget.value()
        else:
            return None

    @property
    def relative(self):
        return self._relativeCB.isChecked()

    @property
    def name(self):
        return self._namesCB.currentText()

    def _setSize(self, size):
        Dim._setSize(self, size)
        self._sizeWidget.setValue(size=size, editable=self.relative)

    def setNames(self, names):
        self._namesCB.clear()
        for name in sorted(list(names)):
            self._namesCB.addItem(name)

    def embedInTable(self, table, row):
        self.__row = row
        for column, widget in enumerate((self._axis, self._kindCB, self._namesCB,
                                         self._sizeWidget, self._relativeCB,
                                         self._infoButton, self._rmButton)):
            table.setCellWidget(row, column, widget)

    def _updateNames(self, *args, **kwargs):
        """Update names for the current kind"""
        if self.__metadata is not None:
            _lastActiveName = self.name
            self.setNames(self.__metadata.get_keys(self.kind))
            idx = self._namesCB.findText(_lastActiveName)
            if idx >= 0:
                self._namesCB.setCurrentIndex(idx)

    def _setMetadata(self, metadata):
        self.__metadata = metadata
        self._updateNames()
