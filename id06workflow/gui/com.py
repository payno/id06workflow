# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "04/10/2018"


import logging
from silx.gui import qt
from silx.gui.plot import Plot2D
from id06workflow.core.mapping import MEAN, VARIANCE, SKEWNESS, KURTOSIS
from id06workflow.core.operation.com import COM as COMOperation
from id06workflow.gui.settings import DEFAULT_COLORMAP

_logger = logging.getLogger(__file__)


class ComWidget(qt.QWidget):
    """
    Widget to display result of the COM operation
    """
    def __init__(self, parent=None, comOperation=None):
        qt.QWidget.__init__(self, parent)
        self.setLayout(qt.QVBoxLayout())
        self._control = _ComControlWidget(parent=self)
        self.layout().addWidget(self._control)
        self._plot = Plot2D(parent=self)
        self._plot.setDefaultColormap(DEFAULT_COLORMAP)
        self.layout().addWidget(self._plot)

        # connect signal
        self._control.sigSelectionChanged.connect(self._update)

        if comOperation is not None:
            self.setOperation(comOperation)

    def setOperation(self, operation):
        assert isinstance(operation, (COMOperation, type(None)))
        self._operation = operation
        self._control.setOperation(operation)

    def _update(self):
        self._plot.clear()
        dim = self._control.getDim()
        map_type = self._control.getMapType()

        if dim is None:
            return

        self._plot.clear()
        try:
            _map = getattr(self._operation.maps[dim], map_type)
            self._plot.addImage(_map)
        except Exception as e:
            _logger.error(e)


class _ComControlWidget(qt.QWidget):

    sigSelectionChanged = qt.Signal()
    """Signal emitted when the selection of the com change"""

    def __init__(self, parent=None):
        qt.QWidget.__init__(self, parent)
        self.setLayout(qt.QHBoxLayout())
        self.layout().addWidget(qt.QLabel('dims:', parent=self))
        self._dim = qt.QComboBox(parent=self)
        self.layout().addWidget(self._dim)
        self.layout().addWidget(qt.QLabel('map type:', parent=self))
        self._map = qt.QComboBox(parent=self)
        self.layout().addWidget(self._map)
        # TODO: are those name correct ?
        for map_type in (MEAN, VARIANCE, SKEWNESS, KURTOSIS):
            self._map.addItem(map_type)
        index = self._map.findText(MEAN)
        assert index >= 0
        self._map.setCurrentIndex(index)

        # connect signals
        self._map.currentIndexChanged.connect(self.__selectionChanged)
        self._dim.currentIndexChanged.connect(self.__selectionChanged)

    def setOperation(self, operation):
        assert isinstance(operation, (COMOperation, type(None)))
        self._dim.clear()
        if operation is not None:
            for dim in range(operation.ndim):
                self._dim.addItem(str(dim))

        self._map.setEnabled(operation is not None and operation.ndim > 0)

    def getDim(self):
        currentDim = self._dim.currentText()
        if currentDim == '':
            return None
        else:
            return int(currentDim)

    def getMapType(self):
        return self._map.currentText()

    def __selectionChanged(self, *args, **kwargs):
        self.sigSelectionChanged.emit()
