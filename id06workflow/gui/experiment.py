# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "16/10/2018"


from silx.gui import qt
from id06workflow.core.experiment import Experiment


class DisplayExperiment(qt.QWidget):
    """
    Widget to display the operation of the experiment.
    In the future we can imagine to show the treated data of the experiment
    stack + z plot and in other tabs the 'Additive operation' which are
    in some way third data of the experiment
    """
    def __init__(self, parent=None):
        self._experiment = None
        qt.QWidget.__init__(self, parent)
        self.setLayout(qt.QVBoxLayout())
        self._list = qt.QListWidget(parent=self)
        self.layout().addWidget(self._list)

    def setExperiment(self, experiment):
        assert isinstance(experiment, (Experiment, None))
        self._experiment = experiment
        self._list.clear()
        if experiment is not None:
            for operation_key in experiment.operations:
                self._list.addItem(operation_key)

    @property
    def experiment(self):
        return self._experiment
