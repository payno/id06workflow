# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "10/10/2018"


from silx.gui.plot import Plot2D
from Orange.widgets import gui
from Orange.widgets.widget import OWWidget
from Orange.canvas.registry.description import InputSignal, OutputSignal
from id06workflow.core.experiment import Experiment
from id06workflow.core.types import _Image
from id06workflow.gui.settings import DEFAULT_COLORMAP
import logging
_logger = logging.getLogger(__file__)


class ZSumPlotOW(OWWidget):
    """
    Simple widget to sum the data over the z axis
    """

    name = "z sum plot"
    id = "orange.widgets.id06workflow.zsum"
    description = "Display the data summed over the z axis"
    icon = "icons/z_sum.svg"

    priority = 8
    category = "esrfWidgets"
    keywords = ["dataset", "plot", "z", "sum"]

    inputs = [InputSignal(name="data", type=Experiment, handler='_process')]
    outputs = [OutputSignal(name="image", type=_Image)]

    want_main_area = True
    resizing_enabled = True
    compress_signal = False

    def __init__(self):
        super().__init__()

        layout = gui.vBox(self.mainArea, 'z sum plot').layout()
        self._plot = Plot2D(parent=self)
        self._plot.setDefaultColormap(DEFAULT_COLORMAP)
        layout.addWidget(self._plot)

    def _process(self, experiment):
        if experiment is not None and experiment.data is not None:
            self._plot.addImage(experiment.data.sum(axis=0))
        else:
            self._plot.clear()
        self.send("image", _Image(self._plot.getActiveImage(just_legend=False), 'zsum'))

