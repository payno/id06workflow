from silx.gui import qt

def screenshot():
    from orangecontrib.id06workflow.widgets.com import ComOW
    from id06workflow.test import utils

    app = qt.QApplication.instance() or qt.QApplication([])
    widget = ComOW()
    # TODO: create a more realsitic dataset or store one on edna-site
    _map = utils.createRandomMap((200, 200))
    widget._process(_map)
    widget.show()
    app.exec_()


if __name__ == '__main__':
    screenshot()
