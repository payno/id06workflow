from silx.gui import qt

def screenshot():
    from orangecontrib.id06workflow.widgets.metadatatable import MetadataTableOW
    import os
    from id06workflow.core.Dataset import Dataset
    from id06workflow.core.experiment import Experiment

    app = qt.QApplication.instance() or qt.QApplication([])
    widget = MetadataTableOW()
    # TODO: create a more realsitic dataset or store one on edna-site
    try:
        root_folder = '/nobackup/linazimov/payno/datasets/id06/strain_scan'
        data_file_pattern = os.path.join(root_folder,
                                         'reduced_strain/strain_0000.edf')
        data_bb_files = []
        bb_folder = os.path.join(root_folder, 'bg_ff_5s_1x1')
        for _file in os.listdir(bb_folder):
            data_bb_files.append(os.path.join(bb_folder, _file))
        dataset = Dataset(data_files_pattern=data_file_pattern,
                          ff_files=data_bb_files)
        experiment = Experiment(dataset=dataset, geometry=None)
        widget._process(experiment)
    except:
        pass
    widget.resize(800, 400)
    widget.show()
    app.exec_()


if __name__ == '__main__':
    screenshot()
