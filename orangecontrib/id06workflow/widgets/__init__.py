import sysconfig
# Category metadata.

# Category icon show in the menu
ICON = "icons/ID06.png"

# Background color for category background in menu
# and widget icon background in workflow.
BACKGROUND = "light-blue"

URL = 'http://www.silx.org/pub/doc/id06workflow'

# Location of widget help files.
intersphinx = (
    # Development documentation
    # You need to build help pages manually using
    # make htmlhelp
    # inside doc folder
    ("{DEVELOP_ROOT}/doc/build/htmlhelp/index.html", None),
    ("{URL}/docs/latest/",
     "{URL}/docs/latest/_objects/")
)
