# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "02/10/2018"

import logging
from silx.gui import qt

from Orange.canvas.registry.description import InputSignal, OutputSignal
from Orange.widgets import gui
from Orange.widgets.widget import OWWidget
from Orange.widgets.settings import Setting
from id06workflow.core.experiment import Experiment
from id06workflow.core.types import _Image
from id06workflow.gui.shifcorrection import ShiftCorrectionWidget

_logger = logging.getLogger(__file__)


class ShiftCorrectionOW(OWWidget):
    """
    Widget used to apply a shift correction. User can define two points which 
     is the same position on browsing dataset.
    """

    name = "Shift correction"
    id = "orange.widgets.id06workflow.shiftcorrection"
    description = "Apply shift correction on the dataset"
    icon = "icons/shift_correction.svg"

    priority = 10
    category = "esrfWidgets"
    keywords = ["dataset", "shift", "correction"]

    inputs = [InputSignal(name="data", type=Experiment, handler='_process')]
    outputs = [OutputSignal(name="data", type=Experiment),
               OutputSignal(name="image", type=_Image)]

    want_main_area = True
    resizing_enabled = True
    compress_signal = False

    _dx = Setting(int())
    _dy = Setting(int())
    _dz = Setting(int())

    def __init__(self):
        super().__init__()
        self._editedExperiment = None
        self._widget = ShiftCorrectionWidget(parent=self)
        self._progress = gui.ProgressBar(self, 100)
        self._widget.sigProgress.connect(self._setProgressValue)
        layout = gui.vBox(self.mainArea, 'data selection').layout()
        layout.addWidget(self._widget)
        layout.addWidget(self._widget)

        # buttons
        types = qt.QDialogButtonBox.Ok
        self._buttons = qt.QDialogButtonBox(parent=self)
        self._buttons.setStandardButtons(types)
        self._updateButton = qt.QPushButton("Update")
        self._buttons.addButton(self._updateButton, qt.QDialogButtonBox.ActionRole)
        self._updateButton.pressed.connect(self._widget._updateShift)

        layout.addWidget(self._buttons)

        self._buttons.hide()
        self._buttons.accepted.connect(self.validate)

        # expose API
        self.setShift = self._widget.setShift

        self._widget.setShift(dx=self._dx, dy=self._dy, dz=self._dz)

    def _setProgressValue(self, value):
        self._progress.widget.progressBarSet(value)

    def _process(self, experiment):
        assert isinstance(experiment, (Experiment, type(None)))
        if experiment.dataset is None or experiment.dataset.is_valid() is False:
            _logger.warning('cannot process shift correction, need a valid dataset for this')
        else:
            _logger.info('Receive some data for shift application')
            self._editedExperiment = experiment
            self._widget.setExperiment(experiment)
            self._buttons.show()
            self.show()

    def validate(self):
        if self._widget.isProcessing() is True:
            _logger.error('you should wait the end of the processing before'
                          'validation')
            return
        self._progress.finish()
        if self._editedExperiment is not None:
            self._widget._updateShift()
            if self._widget.getShift() != (0, 0, 0):
                self._widget.getOperation().apply()

            self.send("data", self._editedExperiment)
            image = _Image(self._widget._sumPlot.getActiveImage(just_legend=False),
                           'shift_correction')
            self.send("image",
                      image)
            # TODO: clear widget to avoid reprocessing ?
            OWWidget.accept(self)

    def updateProperties(self):
        self._dx, self._dy, self._dz = self._widget.getShift()
