# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "01/10/2018"

from silx.gui import qt
from Orange.widgets import gui
from Orange.widgets.widget import OWWidget
from Orange.canvas.registry.description import InputSignal, OutputSignal
from id06workflow.core.experiment import Experiment
from id06workflow.gui.noisereduction import NoiseReductionWidget
import logging

_logger = logging.getLogger(__file__)


class NoiseReductionOW(OWWidget):
    """

    """
    name = "noise reduction"
    id = "orange.widgets.id06workflow.noisereduction"
    description = "Remove noise from the dataset"
    icon = "icons/noise_removal.png"
    priority = 25
    category = "esrfWidgets"
    keywords = ["noise", "reduction", "dataset", "filter"]

    inputs = [InputSignal(name="data", handler='_process', type=Experiment)]
    outputs = [OutputSignal(name="data", type=Experiment)]

    want_main_area = True
    resizing_enabled = True
    compress_signal = False

    def __init__(self):
        super().__init__()

        self._widget = NoiseReductionWidget(parent=self)
        layout = gui.vBox(self.mainArea, 'noise reduction').layout()
        layout.addWidget(self._widget)

        # buttons
        types = qt.QDialogButtonBox.Ok
        self._buttons = qt.QDialogButtonBox(parent=self)
        self._buttons.setStandardButtons(types)
        self._updateButton = qt.QPushButton("Update")
        self._buttons.addButton(self._updateButton, qt.QDialogButtonBox.ActionRole)
        self._updateButton.pressed.connect(self._widget._updateCorrection)

        layout.addWidget(self._buttons)

        self._buttons.hide()
        self._buttons.accepted.connect(self.validate)

        # expose API
        self.setBackgroundSubtraction = self._widget.setBackgroundSubtraction
        self.setLowerThreshold = self._widget.setLowerThreshold
        self.setUpperThreshold = self._widget.setUpperThreshold

    def _process(self, experiment):
        if experiment is None:
            return
        assert isinstance(experiment, Experiment)
        if experiment.data is None:
            _logger.warning('dataset is empty, won\'t apply noise reduction')
        else:
            self._widget.reset(experiment)
            self._buttons.show()
            self.show()

    def validate(self):
        exp = self._widget._experiment
        if exp is not None:
            try:
                self._widget.validate_correction()
            except ValueError:
                # raw fix, in case update has not been activated yet
                # TODO: should be managed by buttons availibility or know
                # if there is some data in the cache
                self._widget._updateCorrection()
                self._widget.validate_correction()

            assert isinstance(exp, Experiment)
            _logger.info('Noise reduction validated')
            self.send("data", exp)
            # For the moment: keep a copy of all
            # self._widget.clearExperiment()
        OWWidget.accept(self)

    @property
    def _editedExperiment(self):
        """
        Used for unit test

        :return: currently edited experiment
        """
        return self._widget._experiment

