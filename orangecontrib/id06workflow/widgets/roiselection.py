# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "02/10/2018"

import logging
from silx.gui import qt

from Orange.canvas.registry.description import InputSignal, OutputSignal
from Orange.widgets import gui
from Orange.widgets.widget import OWWidget
from Orange.widgets.settings import Setting
from id06workflow.core.experiment import Experiment
from id06workflow.core.operation.roi import RoiOperation
from id06workflow.gui.roiselection import ROIOnStackView

_logger = logging.getLogger(__file__)


class RoiSelectionOW(OWWidget):
    """
    Widget used to make the selection of a region of Interest to treat in a
    Dataset.
    """

    name = "ROI selection"
    id = "orange.widgets.id06workflow.roiselection"
    description = "Select data Region Of Interest"
    icon = "icons/image-select-box.svg"

    priority = 10
    category = "esrfWidgets"
    keywords = ["dataset", "data", "selection", "ROI", "Region of Interest"]

    inputs = [InputSignal(name="data", type=Experiment, handler='_process')]
    outputs = [OutputSignal(name="data", type=Experiment)]

    want_main_area = True
    resizing_enabled = True
    compress_signal = False

    _roi_origin = Setting(tuple())
    _roi_size = Setting(tuple())

    def __init__(self):
        super().__init__()

        self._widget = ROIOnStackView(parent=self)
        layout = gui.vBox(self.mainArea, 'data selection').layout()
        layout.addWidget(self._widget)
        self._editedExperiment = None

        # buttons
        types = qt.QDialogButtonBox.Ok
        self._buttons = qt.QDialogButtonBox(parent=self)
        self._buttons.setStandardButtons(types)
        layout.addWidget(self._buttons)

        self._buttons.hide()
        self._buttons.accepted.connect(self.validate)

        # expose API
        self.setROI = self._widget.setROI

        if len(self._roi_size) is 2:
            self._widget.getROI().setSize(size=self._roi_size)
        if len(self._roi_origin) is 2:
            self._widget.getROI().setOrigin(self._roi_origin)

    def _process(self, experiment):
        if experiment.dataset is None or experiment.dataset.is_valid() is False:
            _logger.warning('cannot process roi selection, need a valid dataset for this')
        else:
            assert experiment is not None
            self._editedExperiment = experiment
            self._widget.setStack(self._editedExperiment.data_flatten)
            self._buttons.show()
            self.show()

    def validate(self):
        """
        callback when the ROI has been validated
        """
        if self._editedExperiment is None:
            return

        roi = self._widget.getROI()
        # TODO: try catch is used here because on the future the setROI might
        # be able to raise an exception, especially if the ROI as no sense
        # regarding the data.
        try:
            operation = RoiOperation(experiment=self._editedExperiment,
                                     origin=roi.getOrigin(),
                                     size=roi.getSize())
            operation.compute()
            _logger.info('ROI definition is valid, processing to next boxes')
            self.send("data", self._editedExperiment)
        except Exception as e:
            _logger.error(e)
        else:
            OWWidget.accept(self)
        # TODO: what can be the image to send to show roi selection (z sum ?)

    def updateProperties(self):
        # as dim are named tuple we have to cast them to dict
        self._roi_origin = tuple(self._widget.getROI().getOrigin())
        self._roi_size = tuple(self._widget.getROI().getSize())

        print(self._roi_origin)
        print(self._roi_size)
