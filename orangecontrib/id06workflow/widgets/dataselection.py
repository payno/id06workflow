# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "02/10/2018"


from Orange.widgets import gui
from Orange.widgets.widget import OWWidget
from Orange.canvas.registry.description import OutputSignal
from Orange.widgets.settings import Setting
from id06workflow.gui.datasetselection import DatasetSelection
from id06workflow.core.experiment import Experiment
from silx.gui import qt
import os
import logging
_logger = logging.getLogger(__file__)


class DataSelectionOW(OWWidget):
    """
    TODO: define the graph and the possible acquisition types.
    """
    name = "data selection"
    id = "orange.widgets.id06workflow.dataselection"
    description = "Select data to define the dataset to treat"
    icon = "icons/scanselector.svg"
    priority = 1
    category = "esrfWidgets"
    keywords = ["dataset", "data", "selection"]

    outputs = [OutputSignal(name="data", type=Experiment)]

    want_main_area = True
    resizing_enabled = True
    compress_signal = False

    _data_file_pattern = Setting(str())
    _flat_field_files = Setting(list())
    _dark_files = Setting(list())

    def __init__(self):
        # TODO: add drag and drop stuff on it
        super().__init__()
        self._widget = DatasetSelection(parent=self)
        layout = gui.vBox(self.mainArea, 'data selection').layout()
        layout.addWidget(self._widget)

        # expose API
        self.getDataset = self._widget.getDataset

        # buttons
        types = qt.QDialogButtonBox.Ok
        _buttons = qt.QDialogButtonBox(parent=self)
        _buttons.setStandardButtons(types)
        layout.addWidget(_buttons)

        _buttons.accepted.connect(self._process)

        # expose API
        self.setDataset = self._widget.setDataset

        if os.path.isfile(self._data_file_pattern):
            self._widget.setDataFilesPattern(self._data_file_pattern)
        elif self._data_file_pattern != '':
            _logger.info('%s is not recognized as an existing file pattern' % self._data_file_pattern)
        for _file in self._flat_field_files:
            if os.path.isfile(_file):
                self._widget.addFlatFieldFile(_file)
            else:
                _logger.info('%s is not a valid file path')
        for _file in self._dark_files:
            if os.path.isfile(_file):
                self._widget.addDarkFile(_file)
            else:
                _logger.info('%s is not a valid file path')

    def _process(self):
        """
        Code executed when the dataset has been validated
        """
        dataset = self.getDataset()
        if dataset.is_valid() is False:
            _logger.warning('Defined dataset is invalid. Can\'t process')
        else:
            _logger.info('Dataset definition is valid, processing to next boxes')
            self.send("data", Experiment(dataset=dataset))
        OWWidget.accept(self)

    def sizeHint(self):
        return qt.QSize(400, 400)

    def updateProperties(self):
        self._data_file_pattern = self._widget.getDataFilesPattern()
        self._flat_field_files = self._widget.getFlatFieldFiles()
        self._dark_files = self._widget.getDarkFiles()
