# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "18/10/2018"


import logging

from Orange.canvas.registry.description import OutputSignal, InputSignal
from Orange.widgets import gui
from Orange.widgets.widget import OWWidget
from id06workflow.core.operation.mapping import _MappingBase

from id06workflow.core.experiment import Experiment
from id06workflow.core.operation.com import COM as COMOperation
from id06workflow.gui.com import ComWidget

_logger = logging.getLogger(__file__)


class ComOW(OWWidget):
    """
    Compute Com for the received mapping and update
    """
    name = "COM"
    id = "orange.widgets.id06workflow.com"
    description = "Compute center of mass of the data"
    icon = "icons/com.svg"
    priority = 16
    category = "esrfWidgets"
    keywords = ["dataset", "data", "com", "center", "of", "mass"]

    inputs = [InputSignal(name="map", handler='_process', type=_MappingBase)]
    outputs = [OutputSignal(name="map", type=_MappingBase),
               OutputSignal(name="data", type=Experiment)]

    want_main_area = True
    resizing_enabled = True
    compress_signal = False


    def __init__(self):
        # TODO: add drag and drop stuff on it
        super().__init__()
        layout = gui.vBox(self.mainArea, 'data reduction').layout()
        self._widget = ComWidget(parent=self)
        layout.addWidget(self._widget)

    def _process(self, _map):
        if _map is None:
            return

        assert isinstance(_map, _MappingBase)

        operation = COMOperation(map=_map)
        operation.compute()
        self._widget.setOperation(operation)
        self.send("data", operation.experiment)
        self.send("map", operation)
