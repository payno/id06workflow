# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "01/10/2018"

import logging
from functools import partial

from Orange.canvas.registry.description import InputSignal, OutputSignal
from Orange.widgets import gui
from Orange.widgets.widget import OWWidget
from id06workflow.core.operation.mapping import IntensityMapping, \
    _MappingBase

from id06workflow.core.experiment import Experiment
from id06workflow.core.experiment import _METADATA_TYPES_I
from id06workflow.core.mapping import MEAN, VARIANCE, SKEWNESS, KURTOSIS
from id06workflow.core.operation.ThreadedOperation import ThreadedOperation
from id06workflow.core.types import _Image
from id06workflow.gui.mapping import MappingPlot

_logger = logging.getLogger(__file__)


class MappingOW(OWWidget):
    """

    """
    name = "mapping"
    id = "orange.widgets.id06workflow.mapping"
    description = "Map data with angles"
    icon = "icons/mapping.svg"
    priority = 25
    category = "esrfWidgets"
    keywords = ["noise", "removal", "dataset", "filter"]

    inputs = [InputSignal(name="data", handler='_process', type=Experiment)]
    outputs = [OutputSignal(name="data", type=Experiment),
               OutputSignal(name="image", type=_Image),
               OutputSignal(name='map', type=_MappingBase)]

    want_main_area = True
    resizing_enabled = True
    compress_signal = False

    def __init__(self):
        super().__init__()

        layout = gui.vBox(self.mainArea, 'noise reduction').layout()
        self._progress = gui.ProgressBar(self, 100)

        self._plot = MappingPlot(parent=self)
        layout.addWidget(self._plot)

        self._processing_thread = ThreadedOperation()

    def _setProgressValue(self, value):
        self._progress.widget.progressBarSet(value)

    def _process(self, experiment):
        if experiment is None:
            return

        if self._processing_thread.isRunning():
            _logger.error('Mapping thread is already running, cannot take another'
                          'job yet')
            return
        operation = IntensityMapping(experiment=experiment)
        operation.sigProgress.connect(self._setProgressValue)
        self._processing_thread.init(operation)

        callback = partial(self._post_processing, operation, experiment)
        self._processing_thread.finished.connect(callback)
        self._processing_thread.start()

    def _post_processing(self, operation, experiment):
        if operation.intensity_map is not None:
            self.send("image",
                      _Image(img=operation.intensity_map, name='intensity'))
        for iDim, dim in operation.dims.items():
            name = dim.name
            kind = _METADATA_TYPES_I[dim.kind]
            for mode in (MEAN, VARIANCE, SKEWNESS, KURTOSIS):
                img = getattr(dim, mode)
                img_name = '_'.join((kind, name, mode))
                self.send("image", _Image(img=img, name=img_name))

        self._plot.setOperation(operation)
        self.send("data", experiment)
        self.send("map", operation)

        OWWidget.accept(self)
