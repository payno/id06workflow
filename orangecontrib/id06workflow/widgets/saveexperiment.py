# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "10/10/2018"


from Orange.widgets import gui
from Orange.widgets.widget import OWWidget
from Orange.canvas.registry.description import InputSignal, OutputSignal
from id06workflow.core.experiment import Experiment
from id06workflow.gui.experiment import DisplayExperiment
import logging
_logger = logging.getLogger(__file__)


class SaveExperimentOW(OWWidget):
    """
    Widget used to make the selection of a region of Interest to treat in a
    Dataset.
    """

    name = "save experiment"
    id = "orange.widgets.id06workflow.saveexperiment"
    description = "Save the current experiment treatment"
    icon = "icons/document-save.svg"

    priority = 8
    category = "esrfWidgets"
    keywords = ["dataset", "save"]

    inputs = [InputSignal(name="data", type=Experiment, handler='_process')]
    outputs = [OutputSignal(name="data", type=Experiment)]

    want_main_area = True
    resizing_enabled = True
    compress_signal = False

    def __init__(self):
        super().__init__()
        layout = gui.vBox(self.mainArea, 'experiment').layout()
        self._widget = DisplayExperiment(parent=self)
        layout.addWidget(self._widget)

    def _process(self, experiment):
        if experiment is not None:
            self._widget.setExperiment(experiment)

    @property
    def _editedExperiment(self):
        """
        Used for unit test

        :return: currently edited experiment
        """
        return self._widget.experiment
