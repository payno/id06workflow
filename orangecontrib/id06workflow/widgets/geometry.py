# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "01/10/2018"


from silx.gui import qt
from Orange.widgets import gui
from Orange.widgets.widget import OWWidget
from Orange.canvas.registry.description import InputSignal, OutputSignal
from id06workflow.gui.geometry import GeometryMainWidget
from id06workflow.core.experiment import Experiment


class GeometryOW(OWWidget):
    """
    Widget used to define the calibration of the experimentation (select motor
    positions...)
    """
    name = "Geometry"
    id = "orange.widgets.id06workflow.geometry"
    description = "Define the geometry used for the experimentation"
    icon = "icons/calibration.svg"

    priority = 4
    category = "esrfWidgets"
    keywords = ["dataset", "calibration", "motor", "angle", "geometry"]

    inputs = [InputSignal(name="data", type=Experiment, handler='_process')]
    outputs = [OutputSignal(name="data", type=Experiment)]

    want_main_area = True
    resizing_enabled = True
    compress_signal = False

    def __init__(self):
        super().__init__()
        layout = gui.vBox(self.mainArea, 'geometry').layout()
        self._widget = GeometryMainWidget(parent=self)
        layout.addWidget(self._widget)

        # buttons
        types = qt.QDialogButtonBox.Ok
        _buttons = qt.QDialogButtonBox(parent=self)
        _buttons.setStandardButtons(types)
        layout.addWidget(_buttons)

        _buttons.accepted.connect(self._process)

        # expose API
        self.setSetupGeometry = self._widget.setSetupGeometry
        self.getSetupGeometry = self._widget.getSetupGeometry

    def _process(self, experiment):
        geometry = self._widget.getSetupGeometry()
        if geometry.is_valid() is True:
            experiment.geometry = geometry
            self.send("data", experiment)
        else:
            self.show()
