# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "01/10/2018"


from silx.gui import qt
from Orange.widgets import gui
from Orange.widgets.widget import OWWidget
from Orange.canvas.registry.description import InputSignal, OutputSignal
from Orange.widgets.settings import Setting
from id06workflow.gui.dimensions import DimensionWidget
from id06workflow.core.experiment import Experiment, Dim
from functools import partial
import logging

_logger = logging.getLogger(__file__)


class DimensionOW(OWWidget):
    """
    Widget used to define the calibration of the experimentation (select motor
    positions...)
    """
    name = "dimension definition"
    id = "orange.widgets.id06workflow.dimensiondefinition"
    description = "Define the dimension followed during the acquisition"
    icon = "icons/param_dims.svg"

    priority = 4
    category = "esrfWidgets"
    keywords = ["dataset", "calibration", "motor", "angle", "geometry"]

    inputs = [InputSignal(name="data", type=Experiment, handler='_process')]
    outputs = [OutputSignal(name="data", type=Experiment)]

    want_main_area = True
    resizing_enabled = True
    compress_signal = False

    _dims = Setting(dict())

    def __init__(self):
        super().__init__()
        layout = gui.vBox(self.mainArea, 'dimensions').layout()
        self._widget = DimensionWidget(parent=self)
        layout.addWidget(self._widget)

        # buttons
        types = qt.QDialogButtonBox.Ok
        _buttons = qt.QDialogButtonBox(parent=self)
        _buttons.setStandardButtons(types)
        layout.addWidget(_buttons)

        _buttons.accepted.connect(self.validate)
        _buttons.button(qt.QDialogButtonBox.Ok).setEnabled(False)

        # connect Signal/SLOT
        _callbackValid = partial(_buttons.button(qt.QDialogButtonBox.Ok).setEnabled, True)
        self._widget.fitSucceed.connect(_callbackValid)
        _callbackInvalid = partial(_buttons.button(qt.QDialogButtonBox.Ok).setDisabled, True)
        self._widget.fitFailed.connect(_callbackInvalid)

        # expose API
        self.setDims = self._widget.setDims

        # load properties
        _dims = self._convertDictToDim(self._dims)
        self._widget.setDims(_dims)

    @property
    def _ndim(self):
        return self._widget.ndim

    @property
    def _experiment(self):
        return self._widget.experiment

    def _process(self, experiment):
        if experiment is not None:
            try:
                self._widget.setExperiment(experiment)
            except ValueError as e:
                qt.QMessageBox.warning(self,
                                       'Fail to setup dimension definition',
                                       str(e))
            else:
                self.show()

    def validate(self):
        succeed, msg = self._widget.fit()
        if succeed is False:
            qt.QMessageBox.warning(self, 'Fail dataset dimension definition',
                                   ''.join(('Fail to define coherent dimension values. Reason is ', str(msg))))
        else:
            self.send("data", self._widget.experiment)
            OWWidget.accept(self)

    def updateProperties(self):
        # as dim are named tuple we have to cast them to dict
        self._dims = {}
        if self._widget.ndim is 0:
            return
        else:
            for _axis, _dim in  self._widget.dims.items():
                assert isinstance(_dim, Dim)
                self._dims[_axis] = _dim.to_dict()

    def _convertDictToDim(self, _dict):
        if len(_dict) > 0:
            _dims = {}
            for _axis, _dim in _dict.items():
                assert type(_dim) is dict
                _dims[_axis] = Dim.from_dict(_dim)
            return _dims

        else:
            return {}
