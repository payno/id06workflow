# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "16/10/2018"


from silx.gui import qt
from Orange.widgets import gui
from Orange.widgets.widget import OWWidget
from Orange.canvas.registry.description import InputSignal, OutputSignal
from id06workflow.core.experiment import Experiment, _METADATA_TYPES
import numpy


class MetadataTableOW(OWWidget):
    """

    """
    name = "Metadata table"
    id = "orange.widgets.id06workflow.metadatatable"
    description = "Display "
    icon = "icons/metadata.svg"

    priority = 4
    category = "esrfWidgets"
    keywords = ["metadata", "experiment", "motors"]

    inputs = [InputSignal(name="data", type=Experiment, handler='_process')]

    want_main_area = True
    resizing_enabled = True
    compress_signal = False

    def __init__(self):
        super().__init__()

        layout = gui.vBox(self.mainArea, 'metadata table').layout()
        self._metadataTypesWidget = qt.QWidget(parent=self)
        layout.addWidget(self._metadataTypesWidget)
        self._metadataTypesWidget.setLayout(qt.QHBoxLayout())
        spacer1 = qt.QWidget(self._metadataTypesWidget)
        spacer1.setSizePolicy(qt.QSizePolicy.Expanding,
                              qt.QSizePolicy.Minimum)
        self._metadataTypesWidget.layout().addWidget(spacer1)
        self._metadataTypesWidget.layout().addWidget(
                qt.QLabel('metadata type:', parent=self._metadataTypesWidget)
        )
        self._metadataCB = qt.QComboBox(parent=self._metadataTypesWidget)
        for _metaDType in _METADATA_TYPES:
            self._metadataCB.addItem(_metaDType)
        self._metadataTypesWidget.layout().addWidget(self._metadataCB)
        default_index = self._metadataCB.findText('default')
        assert default_index >= 0
        self._metadataCB.setCurrentIndex(default_index)

        self._table = qt.QTableWidget(parent=self)
        layout.addWidget(self._table)

        # connect Signal/SLOT
        self._metadataCB.currentTextChanged.connect(self._updateView)

    def _process(self, experiment):
        if experiment is None:
            return
        self.__experiment = experiment
        self._updateView()

    def _updateView(self, metadata_type=None):
        if metadata_type is None:
            metadata_type = self._metadataCB.currentText()
        metadata_type = _METADATA_TYPES[metadata_type]

        self._table.clear()
        v_header = []
        [v_header.append(str(i)) for i in range(self.__experiment.nslices)]
        self._table.setRowCount(len(v_header))
        self._table.setVerticalHeaderLabels(v_header)

        columnCount = None
        for row, metadata_slice in enumerate(self.__experiment.metadata):
            keys = metadata_slice.get_keys(kind=metadata_type)
            if columnCount is None:
                self._table.setColumnCount(len(keys))
                self._table.setHorizontalHeaderLabels(keys)
            elif columnCount != len(metadata_slice):
                raise ValueError('Metadata keys are incoherent')

            for column, key in enumerate(keys):
                _item = qt.QTableWidgetItem()
                txt = metadata_slice.get_value(kind=metadata_type, name=key)
                if type(txt) is numpy.ndarray and txt.size is 1:
                    txt = txt[0]
                if hasattr(txt, 'decode'):
                    txt = txt.decode('utf-8')
                else:
                    txt = str(txt)
                _item.setText(txt)
                _item.setFlags(qt.Qt.ItemIsEnabled | qt.Qt.ItemIsSelectable)
                self._table.setItem(row, column, _item)
