# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "10/10/2018"


import os
from Orange.widgets.widget import OWWidget
from Orange.canvas.registry.description import InputSignal
from id06workflow.gui.settings import DEFAULT_COLORMAP
from id06workflow.core.types import _Image
from silx.gui.plot import Plot2D

import logging
_logger = logging.getLogger(__file__)


class SaveImageOW(OWWidget):
    """
    Save image to the folder defined in the os 
    """

    name = "save screenshot"
    id = "orange.widgets.id06workflow.saveimage"
    description = "save image to the folder defined in environment variable " \
                  "'WORKFLOW_OUTPUT'"
    icon = "icons/save_img.svg"

    priority = 8
    category = "esrfWidgets"
    keywords = ["dataset", "save"]

    inputs = [InputSignal(name="image", type=_Image, handler='_process')]

    want_main_area = False
    resizing_enabled = True
    compress_signal = False

    def __init__(self):
        super().__init__()
        #TODO: display the current status of the experiment
        self._experiment = None

    def _process(self, image):
        assert type(image is _Image)
        output = os.environ.get('WORKFLOW_OUTPUT')
        if output is None:
            _logger.warning("No environment variable define under "
                            "'WORKFLOW_OUTPUT'")
            output = os.getcwd()
        _logger.info('save image to %s' % output)
        try:
            # TODO: fix me, do not use the creation of Plot to save the image
            # but a qimage ?
            _name = image.name
            if _name.lower().endswith('.png') is False:
                _name = _name + '.png'
            plot = Plot2D()
            plot.setDefaultColormap(DEFAULT_COLORMAP)
            plot.addImage(image.img)
            plot.replot()
            plot.saveGraph(filename=os.path.join(output, _name), fileFormat='png')
        except Exception as e:
            _logger.error(e)
