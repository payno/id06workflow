# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/
__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "24/01/2017"

import logging
import unittest
from silx.gui import qt
from orangecontrib.id06workflow.test.OrangeWorkflowTest import OrangeWorflowTest
from id06workflow.core.experiment import Dataset, POSITIONER_METADATA
from id06workflow.core.experiment import Dim
from id06workflow.unitsystem import metricsystem
from id06workflow.core.geometry.TwoThetaGeometry import TwoThetaGeometry
import os
import tempfile
import shutil

logging.disable(logging.INFO)

app = qt.QApplication.instance() or qt.QApplication([])


@unittest.skipIf(os.path.exists('/nobackup/linazimov/payno/datasets/id06/strain_scan') is False, reason='Data files not available')
class TestTrueData(OrangeWorflowTest):
    """
    Create dummy workflow just to make sure all the widget can interact together
    then are able to process the treatment
    """
    def setUp(self):
        OrangeWorflowTest.setUp(self)

        dataSelectionNode = self.addWidget(
                'orangecontrib.id06workflow.widgets.dataselection.DataSelectionOW')
        geometryNode = self.addWidget(
                'orangecontrib.id06workflow.widgets.geometry.GeometryOW')
        dimDefNode = self.addWidget(
                'orangecontrib.id06workflow.widgets.dimensions.DimensionOW')
        roiSelectionNode = self.addWidget(
                'orangecontrib.id06workflow.widgets.roiselection.RoiSelectionOW')
        noiseReductionNode = self.addWidget(
                'orangecontrib.id06workflow.widgets.noisereduction.NoiseReductionOW')
        shiftCorrectionNode = self.addWidget(
                'orangecontrib.id06workflow.widgets.shiftcorrection.ShiftCorrectionOW')
        mappingNode = self.addWidget(
                'orangecontrib.id06workflow.widgets.mapping.MappingOW')
        saveNode = self.addWidget(
                'orangecontrib.id06workflow.widgets.saveexperiment.SaveExperimentOW')
        screenshotNode = self.addWidget(
                'orangecontrib.id06workflow.widgets.saveimage.SaveImageOW')

        self.processOrangeEvents()

        self.link(dataSelectionNode, "data", geometryNode, "data")
        self.link(geometryNode, "data", dimDefNode, "data")
        self.link(dimDefNode, "data", roiSelectionNode, "data")
        self.link(roiSelectionNode, "data", noiseReductionNode, "data")
        self.link(noiseReductionNode, "data", shiftCorrectionNode, "data")
        self.link(shiftCorrectionNode, "data", mappingNode, "data")
        self.link(mappingNode, "data", saveNode, "data")
        self.link(mappingNode, "image", screenshotNode, "image")
        self.processOrangeEvents()

        self._selectionWidget = self.getWidgetForNode(dataSelectionNode)
        self._geometryWidget = self.getWidgetForNode(geometryNode)
        self._dimDefWidget = self.getWidgetForNode(dimDefNode)
        self._roiSelectionWidget = self.getWidgetForNode(roiSelectionNode)
        self._shiftCorrectionWidget = self.getWidgetForNode(shiftCorrectionNode)
        self._noiseReductionWidget = self.getWidgetForNode(noiseReductionNode)
        self._mappingWidget = self.getWidgetForNode(mappingNode)
        self._saveWidget = self.getWidgetForNode(saveNode)

        self._dataset = self.createDataset()

        self._geometry = TwoThetaGeometry(
                twotheta=0.03, # for now those are defined in degree but should
                # be defined in radians
                xmag=1.0, # what is the unit of this ?
                xpixelsize=1.0*metricsystem.micrometer, # TODO: unit
                ypixelsize=1.0*metricsystem.micrometer,
                orientation=TwoThetaGeometry.VERTICAL)

        self._screenshot_output = tempfile.mkdtemp()
        self._previous_workflow_output = os.environ.get('WORKFLOW_OUTPUT')
        os.environ['WORKFLOW_OUTPUT'] = self._screenshot_output

    def tearDown(self):
        if self._previous_workflow_output is not None:
            os.environ['WORKFLOW_OUTPUT'] = self._previous_workflow_output

        shutil.rmtree(self._screenshot_output)
        OrangeWorflowTest.tearDown(self)

    def createDataset(self):
        root_folder = '/nobackup/linazimov/payno/datasets/id06/strain_scan'
        data_file_pattern = os.path.join(root_folder, 'reduced_strain/strain_0000.edf')
        assert os.path.exists(data_file_pattern)
        data_bb_files = []
        bb_folder = os.path.join(root_folder, 'bg_ff_5s_1x1')
        for _file in os.listdir(bb_folder):
            data_bb_files.append(os.path.join(bb_folder, _file))
        self.dataset = Dataset(data_files_pattern=data_file_pattern,
                               ff_files=data_bb_files)

        dim1 = Dim(kind=POSITIONER_METADATA, name='diffry',
                   relative_prev_val=True, size=31)
        dim2 = Dim(kind=POSITIONER_METADATA, name='obpitch')
        self._dims = {0: dim1, 1: dim2}

        return self.dataset

    def _moveToNextStep(self):
        while app.hasPendingEvents():
            for i in range(5):
                app.processEvents()
                self.qWait(100)

    def test(self):
        # define the dataset
        self._selectionWidget.setDataset(self._dataset)
        self._moveToNextStep()

        # define geometry
        self._geometryWidget.setSetupGeometry(self._geometry)
        self._moveToNextStep()
        self.assertTrue(self._geometryWidget.getSetupGeometry() == self._geometry)
        self._selectionWidget._process()
        self._moveToNextStep()

        # define dimension definition
        self._dimDefWidget.setDims(self._dims)
        self._moveToNextStep()
        self.assertTrue(self._dimDefWidget._widget.dims[0].kind == POSITIONER_METADATA)
        self.assertTrue(self._dimDefWidget._widget.dims[0].name == 'diffry')
        self.assertTrue(self._dimDefWidget._widget.dims[0]._sizeWidget._active)
        self.assertFalse(self._dimDefWidget._widget.dims[1]._sizeWidget._active)
        self.assertTrue(self._dimDefWidget._widget.dims[0].size is 31)
        self.assertTrue(self._dimDefWidget._widget.dims[1].size is None)
        self._dimDefWidget._widget.fit()

        self.assertTrue(self._dimDefWidget._widget.dims[0].size is 31)
        self.assertTrue(len(self._dimDefWidget._widget.dims[0].unique_values) is 31)
        self.assertTrue(self._dimDefWidget._widget.dims[1].size is 2)
        self.assertTrue(len(self._dimDefWidget._widget.dims[1].unique_values) is 2)
        self.assertTrue(self._dimDefWidget._experiment.data.shape == (31, 2, 2048, 2048))
        self._dimDefWidget.validate()
        self._moveToNextStep()

        # define noise reduction
        self._noiseReductionWidget.setBackgroundSubtraction(True)
        self._noiseReductionWidget.setLowerThreshold(5.0)
        self._noiseReductionWidget.setUpperThreshold(5000)
        self._noiseReductionWidget.validate()
        self._moveToNextStep()

        # manage roi selection
        self._roiSelectionWidget.setROI(origin=(750, 750), size=(600, 600))
        self._roiSelectionWidget.validate()
        self._moveToNextStep()

        # manage noise reduction
        self._noiseReductionWidget.validate()
        self._moveToNextStep()

        # manage shift correction
        self.assertTrue(self._shiftCorrectionWidget._editedExperiment is not None)
        self._shiftCorrectionWidget.setShift(dx=0.0, dy=0.0, dz=0.0)
        self._shiftCorrectionWidget.validate()
        self._moveToNextStep()

        # screenshot saving
        # make sur all screenshots are processed
        max_iter = 200
        iter = 0
        while iter < max_iter and os.path.exists(os.path.join(self._screenshot_output, 'intensity.png')) is False:
            self._moveToNextStep()
            iter = iter + 1
            self.qWait(200)
        self.assertTrue(os.path.exists(os.path.join(self._screenshot_output, 'intensity.png')))
        self.assertTrue(os.path.exists(os.path.join(self._screenshot_output, 'positioner_diffry_mean.png')))
        self.assertTrue(os.path.exists(os.path.join(self._screenshot_output, 'positioner_diffry_variance.png')))
        self.assertTrue(os.path.exists(os.path.join(self._screenshot_output, 'positioner_diffry_skewness.png')))
        self.assertTrue(os.path.exists(os.path.join(self._screenshot_output, 'positioner_diffry_kurtosis.png')))
        self.assertTrue(os.path.exists(os.path.join(self._screenshot_output, 'positioner_obpitch_mean.png')))
        self.assertTrue(os.path.exists(os.path.join(self._screenshot_output, 'positioner_obpitch_variance.png')))
        self.assertTrue(os.path.exists(os.path.join(self._screenshot_output, 'positioner_obpitch_skewness.png')))
        self.assertTrue(os.path.exists(os.path.join(self._screenshot_output, 'positioner_obpitch_kurtosis.png')))


def suite():
    test_suite = unittest.TestSuite()
    for ui in (TestTrueData,):
        test_suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(ui))
    return test_suite


if __name__ == '__main__':
    unittest.main(defaultTest=suite())
