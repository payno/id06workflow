# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/
__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "24/01/2017"

import logging
import unittest
import tempfile
from silx.gui import qt
from orangecontrib.id06workflow.test.OrangeWorkflowTest import OrangeWorflowTest
from id06workflow.core.experiment import Dataset
from id06workflow.unitsystem import metricsystem
from id06workflow.core.geometry.TwoThetaGeometry import TwoThetaGeometry
from id06workflow.test import utils
import os

logging.disable(logging.INFO)

app = qt.QApplication.instance() or qt.QApplication([])


class TestFirstSetup(OrangeWorflowTest):
    """
    Create dummy workflow just to make sure all the widget can interact together
    then are able to process the treatment
    """
    def setUp(self):
        OrangeWorflowTest.setUp(self)

        dataSelectionNode = self.addWidget(
                'orangecontrib.id06workflow.widgets.dataselection.DataSelectionOW')
        geometryNode = self.addWidget(
                'orangecontrib.id06workflow.widgets.geometry.GeometryOW')
        roiSelectionNode = self.addWidget(
                'orangecontrib.id06workflow.widgets.roiselection.RoiSelectionOW')
        shiftCorrectionNode = self.addWidget(
                'orangecontrib.id06workflow.widgets.shiftcorrection.ShiftCorrectionOW')
        noiseReductionNode = self.addWidget(
                'orangecontrib.id06workflow.widgets.noisereduction.NoiseReductionOW')
        saveNode = self.addWidget(
                'orangecontrib.id06workflow.widgets.saveexperiment.SaveExperimentOW')
        # TODO: add processing in here

        self.processOrangeEvents()

        self.link(dataSelectionNode, "data", geometryNode, "data")
        self.link(geometryNode, "data", roiSelectionNode, "data")
        self.link(roiSelectionNode, "data", shiftCorrectionNode, "data")
        self.link(shiftCorrectionNode, "data", noiseReductionNode, "data")
        self.link(noiseReductionNode, "data", saveNode, "data")
        self.processOrangeEvents()

        self._selectionWidget = self.getWidgetForNode(dataSelectionNode)
        self._geometryWidget = self.getWidgetForNode(geometryNode)
        self._roiSelectionWidget = self.getWidgetForNode(roiSelectionNode)
        self._shiftCorrectionWidget = self.getWidgetForNode(shiftCorrectionNode)
        self._noiseReductionWidget = self.getWidgetForNode(noiseReductionNode)
        self._saveWidget = self.getWidgetForNode(saveNode)

        self._dataset = utils.createRandomDataset(_dir=tempfile.mkdtemp(),
                                                  dims=(100, 100),
                                                  nb_data_files=10,
                                                  nb_dark_files=2,
                                                  nb_ff_file=1)
        self._geometry = TwoThetaGeometry(
                twotheta=17.0, # for now those are defined in degree but should
                # be defined in radians
                xmag=1.0, # what is the unit of this ?
                xpixelsize=1.0*metricsystem.micrometer, # TODO: unit
                ypixelsize=1.0*metricsystem.micrometer,
                orientation=TwoThetaGeometry.VERTICAL)

    def tearDown(self):
        OrangeWorflowTest.tearDown(self)

    def _moveToNextStep(self):
        app.processEvents()
        import time
        time.sleep(0.3)
        app.processEvents()

    def test(self):
        """Simple creation of the following workflow:
        data selection -> geometry -> ROI selection -> noise reduction
           -> shift correction
        """
        # define the dataset
        self._selectionWidget.setDataset(self._dataset)
        app.processEvents()

        self.assertTrue(self._selectionWidget.getDataset() == self._dataset)

        # define geometry
        self._geometryWidget.setSetupGeometry(self._geometry)
        self._moveToNextStep()
        self.assertTrue(self._geometryWidget.getSetupGeometry() == self._geometry)
        self._selectionWidget._process()
        self._moveToNextStep()

        # define noise reduction
        self._noiseReductionWidget.setBackgroundSubtraction(True)
        self._noiseReductionWidget.setLowerThreshold(0.2)
        self._noiseReductionWidget.setUpperThreshold(0.8)

        # manage roi selection
        self._roiSelectionWidget.setROI(origin=(10, 10), size=(20, 35))
        self._moveToNextStep()
        self._roiSelectionWidget.validate()
        self._moveToNextStep()

        # manage shift correction
        self.assertTrue(self._shiftCorrectionWidget._editedExperiment is not None)
        self._shiftCorrectionWidget.setShift(dx=1.0, dy=-1.0, dz=0.0)
        timeout = 10*1000 # timeout in millisec
        waiting_time = 0
        # wait for shift to be processed
        while (self._shiftCorrectionWidget._widget._operation_thread.isRunning() and
                       waiting_time < timeout):
            self.qWait(200)
            waiting_time += 200
        self._shiftCorrectionWidget.validate()
        self._moveToNextStep()

        # manage noise reduction
        self.assertTrue(self._noiseReductionWidget._editedExperiment is not None)
        self._noiseReductionWidget.validate()
        self._moveToNextStep()

        noise_reducted_data = self._saveWidget._editedExperiment.data
        self.assertTrue(noise_reducted_data.min() >= 0.2)
        self.assertTrue(noise_reducted_data.max() <= 0.8)


def suite():
    test_suite = unittest.TestSuite()
    for ui in (TestFirstSetup, ):
        test_suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(ui))
    return test_suite


if __name__ == '__main__':
    unittest.main(defaultTest="suite")
