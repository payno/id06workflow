"""RST directive to include snapshot of a Qt application in Sphinx doc.

Configuration variable in conf.py:

- snapshotqt_image_type: image file extension (default 'png').
- snapshotqt_script_dir: relative path of the root directory for scripts from
  the documentation source directory (i.e., the directory of conf.py)
  (default: '..').
"""

from __future__ import absolute_import

import logging
import urllib
import sys

from docutils.parsers.rst.directives.images import Figure, Image
from docutils import nodes
import os
from docutils.parsers.rst import directives
try: # check for the Python Imaging Library
    import PIL.Image
except ImportError:
    try:  # sometimes PIL modules are put in PYTHONPATH's root
        import Image
        class PIL(object): pass  # dummy wrapper
        PIL.Image = Image
    except ImportError:
        PIL = None

logging.basicConfig()
_logger = logging.getLogger(__name__)

# TODO:
# - Create image in build directory
# - Check if it is needed to patch block_text?

# RST directive ###############################################################
SNAPSHOTS_QT = os.path.join('snapshotsqt_directive')


class SnapshotQtLink(Figure):
    """
    TODO
    """

    def align(argument):
        return directives.choice(argument, Figure.align_h_values)

    def figwidth_value(argument):
        if argument.lower() == 'image':
            return 'image'
        else:
            return directives.length_or_percentage_or_unitless(argument, 'px')

    option_spec = Image.option_spec.copy()
    option_spec['figwidth'] = figwidth_value
    option_spec['figclass'] = directives.class_option
    option_spec['align'] = align
    has_content = True

    def run(self):
        env = self.state.document.settings.env

        # Create an image filename from arguments
        image_ext = env.config.snapshotqt_image_type.lower()
        image_name = '_'.join(self.arguments) + '.' + image_ext
        image_name = image_name.replace('./\\', '_')
        image_name = ''.join([c for c in image_name
                              if c.isalnum() or c in '_-.'])
        snapshot_dir = os.path.join(env.app.outdir, SNAPSHOTS_QT)
        image_name = os.path.join(snapshot_dir, image_name)
        self.arguments = [os.sep + image_name]
        return super(SnapshotQtLink, self).run()


def setup(app):
    app.add_directive('snapshotqt_link', SnapshotQtLink)
    return {'version': '0.1'}

