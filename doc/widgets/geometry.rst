Geometry
========

.. snapshotqt:: orangecontrib/id06workflow/widgets/screenshots/geometry_screenshot.py


Signals
-------

- (Experiment)

**Outputs**:

- (Experiment)

Description
-----------

Define the geometry of the experiment.
Used especially to feat dimension and define missing metadata
