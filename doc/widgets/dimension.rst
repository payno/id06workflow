dimension definition
====================

.. snapshotqt:: orangecontrib/id06workflow/widgets/screenshots/dimension_definition_screenshot.py



Signals
-------

- (Experiment)

**Outputs**:

- (Experiment)

Description
-----------

Used to define the dimension in order to interpret correctly the dataset.
