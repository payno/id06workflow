Shift Correction
================

.. snapshotqt:: orangecontrib/id06workflow/widgets/screenshots/shift_screenshot.py



Signals
-------

- (Data)

**Outputs**:

- (Data, Image)

Description
-----------

generate image shift
