COM
===

.. snapshotqt:: orangecontrib/id06workflow/widgets/screenshots/com_screenshot.py


Signals
-------

- (map)

**Outputs**:

- (map, data)

Description
-----------

Compute and display Center of Mass
