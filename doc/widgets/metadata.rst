Mapping
=======

.. snapshotqt:: orangecontrib/id06workflow/widgets/screenshots/metadata_table_screenshot.py



Signals
-------

- (Experiment)

**Outputs**:

- no output

Description
-----------

Display the metadata contained in the dataset
