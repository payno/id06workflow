=============
ID06 Workflow
=============

TODO: goal and description

Contents:

.. toctree::
   :maxdepth: 2

Widgets
-------

.. toctree::
   :maxdepth: 1

   widgets/com
   widgets/datareduction
   widgets/dataselection
   widgets/dimension
   widgets/geometry
   widgets/mapping
   widgets/metadata
   widgets/noisereduction
   widgets/roiselection
   widgets/saveexperiment
   widgets/saveimage
   widgets/shiftcorrection
